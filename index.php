<?php get_header(); ?>

<div id="Page">

<?php include( TEMPLATEPATH . '/head.php' ); ?>
<!-- ▽メインコンテンツここから // -->
<section id="Content" class="g-content">
<div class="pagettl u-pc">
<h1><img src="<?php echo get_template_directory_uri(); ?>/images/news/common/page_ttl.png" width="242" height="29" alt="こぐまえんブログ"></h1>
<!-- .pagettl // --></div>
<div class="pagettl u-sp">
<h1><img src="<?php echo get_template_directory_uri(); ?>/images/news/common/sp/page_ttl.jpg" width="640" height="180" alt="こぐまえんブログ"></h1>
<!-- .pagettl // --></div>
<section id="Main" class="category">

<nav id="pankuzu">
	<div class="waku">
		<div class="breadcrumbs" vocab="http://schema.org/" typeof="BreadcrumbList">
		<ul>
			<?php if(function_exists('bcn_display'))
			{
			bcn_display();
			}?>
		</ul>
		</div>
	</div>
</nav>

<div id="entryWrap" class="waku">
	<div id="entryMain">
		<div class="news-list">
			<ul id="entryList" class="fixBox">
				<?php $paged = get_query_var('paged') ? get_query_var('paged') : 1 ; ?>
				<?php
				$args = array(
				'post_type' => 'post',
				'posts_per_page' => 12,
				'order'=>'DESC',
				'paged' => $paged
				);
				$the_query = new WP_Query($args);
				?>
				<?php if(have_posts()): while(have_posts()): the_post();?>
				<?php
					$category = get_the_category();
					$cat_id   = $category[0]->cat_ID;
					$cat_name = $category[0]->cat_name;
					$cat_slug = $category[0]->category_nicename;
				?>
				<li class="fixItem">
					<a href="<?php the_permalink(); ?>">
						<div class="img-entry">
							<?php if(has_post_thumbnail()): ?>
							<figure><?php the_post_thumbnail('thumbnail'); ?></figure>
							<?php else: ?>
							<img src="<?php echo get_template_directory_uri(); ?>/images/noimages.png" Width="200" height="200" alt="NO IMAGE">
							<?php endif; ?>
						</div>
						<?php
							$cats = get_the_category();
							foreach($cats as $cat):
							echo "<span class=\"cat\">".$cat->cat_name."</span>";
							// if($cat->parent) {
							// echo "<span class=\"cat\">".$cat->cat_name."</span>";
							// }
							endforeach;
						?>
						<span class="time"><time datetime="<?php the_time( 'Y.m.d' ); ?>" pubdate="pubdate"><?php the_time( 'Y/m/d' ); ?></time></span>
						<h2 class="ttl-entry"><?php the_title(); ?></h2>
					</a>
				</li>
				<?php endwhile; endif; ?>
			<!-- #entryList // --></ul>

			<div id="pagenav">
				<?php if(function_exists(wp_pagenavi)) {
					wp_pagenavi();
				} ?>
			<!-- #pagenav // --></div>
			<?php wp_reset_postdata(); ?>

		<!-- .news-list // --></div>
	<!-- #entryMain // --></div>

	<div id="entrySide">
		<div class="side-box side-category">
			<h3 class="side-ttl u-pc"><img src="<?php echo get_template_directory_uri(); ?>/images/news/common/ttl_category.png" width="230" height="55" alt="カテゴリー"></h3>
			<h3 class="side-ttl u-sp"><img src="<?php echo get_template_directory_uri(); ?>/images/news/common/sp/ttl_category.png" width="640" height="76" alt="カテゴリー"></h3>
			<ul class="side-list-cont">
			<?php
				$categories = get_categories('parent=1');
				foreach($categories as $val){
					$cat_link = get_category_link($val->cat_ID);
					$post_id = 'category_' . $val -> cat_ID;
					$icon_src = wp_get_attachment_image($attachment_id,'full');
					echo '<li>';
					echo '<div class="overflow"><a href="' . $cat_link . '">' . $val -> name . '</a>';
					$child_cat_num = count(get_term_children($val->cat_ID,'category'));
					if($child_cat_num > 0){
						echo '<ul>';
						$category_children_args = array('parent'=>$val->cat_ID);
						$category_children = get_categories($category_children_args);
						foreach($category_children as $child_val){
							$cat_link = get_category_link($child_val -> cat_ID);
							echo '<li><a href="' . $cat_link . '">' . $child_val -> name . '</a>';
						}
						echo '</ul>';
					}
					echo '</div></li>';
				}
			?>
			</ul>
		<!-- .side-box // --></div>

		<div class="side-box side-archives">
			<h3 class="side-ttl u-pc"><img src="<?php echo get_template_directory_uri(); ?>/images/news/common/ttl_archives.png" width="230" height="55" alt="アーカイブ"></h3>
			<h3 class="side-ttl u-sp"><img src="<?php echo get_template_directory_uri(); ?>/images/news/common/sp/ttl_archives.png" width="640" height="76" alt="アーカイブ"></h3>
			<ul class="side-list-cont">
				<?php wp_get_archives('type=monthly&show_post_count=true'); ?>
			</ul>
		<!-- .side-box // --></div>

		<div class="side-box side-newentry">
			<h3 class="side-ttl u-pc"><img src="<?php echo get_template_directory_uri(); ?>/images/news/common/ttl_newentry.png" width="230" height="55" alt="最近の記事"></h3>
			<h3 class="side-ttl u-sp"><img src="<?php echo get_template_directory_uri(); ?>/images/news/common/sp/ttl_newentry.png" width="640" height="76" alt="最近の記事"></h3>
			<ul class="side-list-cont">
				<?php
				$posts = get_posts(array(
				'post_type' => 'post',
				'posts_per_page' => 5,
				'order'=>'DESC'
				));
				?>
				<?php if($posts): foreach($posts as $post): setup_postdata($post); ?>
				<li>
					<span class="time"><time datetime="<?php the_time( 'Y.m.d' ); ?>" pubdate="pubdate"><?php the_time( 'Y/m/d' ); ?></time></span>
					<h4 class="ttl-entry"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
				</li>
				<?php endforeach; endif; ?>
				<?php wp_reset_postdata(); ?>
			</ul>
		<!-- .side-box // --></div>
	<!-- #entrySide // --></div>
<!-- #entryWrap // --></div>

<!-- #Main // --></section>
<!-- #Content // --></section>
<!-- △メインコンテンツここまで // -->

<?php get_footer(); ?>

<!-- #Page // --></div>

<?php include( TEMPLATEPATH . '/gr_tag.php' ); ?>

</body>
</html>