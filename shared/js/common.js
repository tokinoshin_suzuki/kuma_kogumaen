$(function(){
/*-------------------------------------------
  @ Show current page
--------------------------------------------*/
    // if(location.pathname != "/" && location.pathname != "/privacy/"){

    //   $('.gnavi a[href^="/' + location.pathname.split("/")[1] + '"]').addClass('active');
    //   $activeNavi = $('.gnavi a.active img');
    // $activeNavi.attr('src', $activeNavi.attr('src').replace('.png', '_on.png'));
    // $('.gnavi a.active').removeClass('rover');
    // } else $('.gnavi a').removeClass('active');


/*-------------------------------------------
  @ Image switching at hover
--------------------------------------------*/
  $('.rover').find('img').hover(
    function(){
      $(this).attr('src', $(this).attr('src').replace('.png', '_on.png'));
    },
    function(){
      $(this).attr('src', $(this).attr('src').replace('_on.png', '.png'));
    })
  //マウスオン画像のプリロード
  .each(function(){
    $('<img>').attr('src', $(this).attr('src').replace('.png', '_on.png'));
  });

/*-------------------------------------------
  @ Open a global navi
--------------------------------------------*/
  var $mMenu01 = $('#megamenu01');
  var $mMenu02 = $('#megamenu02');
  var $mMenu03 = $('#megamenu03');

  $('.js-megaMenu-trigger01').hover(
      function(){
        $mMenu01.addClass('is-active');
      },
      function(){
        $mMenu01.removeClass('is-active');
      });
  $('.js-megaMenu-trigger02').hover(
      function(){
        $mMenu02.addClass('is-active');
      },
      function(){
        $mMenu02.removeClass('is-active');
      });
  $('.js-megaMenu').hover(
    function(){
      $(this).addClass('is-active');
    },
    function(){
      $(this).removeClass('is-active');
    });
  $('.gnavi-smallClass a').click(function(){
      $('.gnavi-smallClass').removeClass('is-active');
  })
/*-------------------------------------------
  @ Display contact button
--------------------------------------------*/
  var showFlag = false;
  var hiddenBtn = $('.btn-hidden');
  hiddenBtn.css('right', '-200px');
  //2000pxスクロールで表示
  $(window).scroll(function(){
    if($(this).scrollTop() > 1000){
      if(showFlag == false){
        showFlag = true;
        hiddenBtn.stop().animate({'right': '-140px'}, 400);
      }
    }else{
      if(showFlag){
        showFlag = false;
        hiddenBtn.stop().animate({'right': '-200px'}, 500);
      }
    }
  });
/*-------------------------------------------
  @ Open a SP menu
--------------------------------------------*/
  $('.sp-toggle').click(function(){
    $(this).toggleClass('is-active');
    $('.gnavi-sp').toggleClass('is-active');
    $('.overlay').toggleClass('is-active');
  });
  $('.toggle-caregory').click(function(){
    $(this).toggleClass('is-active');
    $(this).next().toggleClass('is-active');
    $(this).next().slideToggle();
  });
  $('.gnavi-sp a').click(function(){
    $('.toggle-caregory').removeClass('is-active');
    $('.gnavi-sp').removeClass('is-active');
    $('.sp-toggle').removeClass('is-active');
    $('.category-small').removeClass('is-active');
    $('.overlay').removeClass('is-active');
    $('.category-small').slideUp();
  });
  var activeNavi = function(target){
    $('target img').attr('src', $(this).attr('src').replace('.png', '_on.png'));
  };

})