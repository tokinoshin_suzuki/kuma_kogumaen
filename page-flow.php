<?php get_header(); ?>
<div id="Page">

<?php include( TEMPLATEPATH . '/head.php' ); ?>

<!-- ▽メインコンテンツここから // -->
<section id="Content" class="g-content">
<div class="pagettl u-pc">
<h1><img src="<?php echo get_template_directory_uri(); ?>/images/flow/common/page_ttl.png" width="121" height="30" alt="入園案内"></h1>
<!-- .pagettl // --></div>
<div class="pagettl u-sp">
<h1><img src="<?php echo get_template_directory_uri(); ?>/images/flow/common/sp/page_ttl.jpg" width="640" height="180" alt="入園案内"></h1>
<!-- .pagettl // --></div>

<section id="Main">

<div class="header-txt">
<p><img src="<?php echo get_template_directory_uri(); ?>/images/flow/index/ttl_catch.gif" width="542" height="23" alt="こぐまえんで一緒にあそぶおトモダチを待ってます。"></p>
<p>ご入園についてご案内しております。<br>
ご不明な点がございましたらお気軽に<a href="<?php echo get_home_url(); ?>/contact/">お問い合わせ</a>、<br class="u-pc">
またはお電話にてご連絡ください。</p>
<p class="u-sp"><img src="<?php echo get_template_directory_uri(); ?>/images/flow/index/sp/img_catch.jpg" width="402" height="370"></p>
<!-- .header-txt // --></div>

<div class="secttl">
<h2><img src="<?php echo get_template_directory_uri(); ?>/images/flow/index/ttl_section01.png" Width="370" height="42" alt="募集要項"></h2>
<!-- .secttl // --></div>

<div class="requirements">
<table class="item-table">
<tbody>
<tr>
<th width="30%">対象年齢</th>
<td width="70%">生後6ヶ月から3歳未満の乳幼児<br>（在籍する年度の4月1日時点での年齢）</td>
</tr>
<tr>
<th>定員</th>
<td>10名程度（1・2 階の合計）</td>
</tr>
<tr>
<th>保育時間</th>
<td>基本保育 / 7:00〜18:00<br>延長保育 / 18:00〜19:00</td>
</tr>
<tr>
<th>開園日</th>
<td>月曜日〜土曜日</td></tr>
<tr><th>休園日</th>
<td>日曜・祝日・年末年始</td></tr>
</tbody>
</table>
<!-- .requirements // --></div>

<div class="secttl">
<h2><img src="<?php echo get_template_directory_uri(); ?>/images/flow/index/ttl_section02.png" Width="404" height="42" alt="入園の流れ"></h2>
<!-- .secttl // --></div>

<div class="flow">
<div class="flow-box">
	<ul class="item">
    	<li class="item-gray">
        	<div class="item-num">
            <img src="<?php echo get_template_directory_uri(); ?>/images/flow/index/ico_num01.png" width="53" height="53" alt="1">
            <!-- .item-txt // --></div>
            <div class="item-txt">
            	<p class="item-ttl"><img src="<?php echo get_template_directory_uri(); ?>/images/flow/index/sp/ico_num01.png" alt="1" class="u-sp">入園申請のご連絡</p>
                <p>こぐまえんまでご連絡ください。</p>
                <p class="item-tel"><span><a href="tel:078-381-5824">078-381-5824</a></span></p>
                <p>受付時間：月曜～金曜（祝祭日を除く）9:00～17:00</p>
                <p>※入園希望日の1ヶ月前までにご連絡ください。</p>
                <p>※保育中は電話に出れない可能性もございますので、<br class="u-sp">その際は担当者から折り返しご連絡いたします。</p>
            <!-- .item-txt // --></div>
        </li>
        <li>
        	<div class="item-num">
            <img src="<?php echo get_template_directory_uri(); ?>/images/flow/index/ico_num02.png" width="53" height="53" alt="2">
            <!-- .item-txt // --></div>
            <div class="item-txt">
            	<p class="item-ttl"><img src="<?php echo get_template_directory_uri(); ?>/images/flow/index/sp/ico_num02.png" alt="2" class="u-sp">内定通知を交付</p>
            <!-- .item-txt // --></div>
        </li>
        <li class="item-gray">
        	<div class="item-num">
            <img src="<?php echo get_template_directory_uri(); ?>/images/flow/index/ico_num03.png" width="53" height="53" alt="3">
            <!-- .item-txt // --></div>
            <div class="item-txt">
            	<p class="item-ttl"><img src="<?php echo get_template_directory_uri(); ?>/images/flow/index/sp/ico_num03.png" alt="1" class="u-sp">各自にて健康診断</p>
            <!-- .item-txt // --></div>
        </li>
        <li>
        	<div class="item-num">
            <img src="<?php echo get_template_directory_uri(); ?>/images/flow/index/ico_num04.png" width="53" height="53" alt="4">
            <!-- .item-txt // --></div>
            <div class="item-txt">
            	<p class="item-ttl"><img src="<?php echo get_template_directory_uri(); ?>/images/flow/index/sp/ico_num04.png" alt="1" class="u-sp">入園前親子面談</p>
                <dl>
                	<dt class="item-subttl">親子面談時必要書類</dt>
                    <dd>
                    <ul class="item-list">
                        <li>利用契約書</li>
                        <li>園児生活調査票</li>
                        <li>育児方針について</li>
                    </ul>
                    <ul class="item-list">
                        <li>母子手帳　健康診断書　乳児医療証の写真</li>
                        <li>健康診断書</li>
                    </ul>
                    <ul class="item-list">
                        <li>利用予定表</li>
                        <li>印鑑</li>
                    </ul>
                    </dd>
                </dl>
            <!-- .item-txt // --></div>
        </li>
    </ul>
<!-- .flow-box // --></div>

<div class="document">
<h3 class="document-ttl">入園申請時必要書類</h3>
<ul class="document-list">
	<li>入園申請書</li>
    <li>健康報告書</li>
    <li>アレルギー確認票</li>
    <li>収入証明貼付票</li>
</ul>
<ul class="document-list">
    <li>健康保険証の写し</li>
    <li>お迎えの方の写真貼付票</li>
</ul>
<!-- .document // --></div>
<!-- .flow // --></div>

<div class="footer-nav">
<ul>
	<li><a href="<?php echo get_home_url(); ?>/concept/" class="blue-btn btn-shadow"><span><img src="<?php echo get_template_directory_uri(); ?>/shared/images/btn_concept.png" class="u-pc" alt="保育理念"><img src="<?php echo get_template_directory_uri(); ?>/shared/images/sp/btn_concept.png" class="u-sp" alt="保育理念"></span></a></li>
    <li><a href="<?php echo get_home_url(); ?>/life/" class="red-btn btn-shadow"><span><img src="<?php echo get_template_directory_uri(); ?>/shared/images/btn_life.png" class="u-pc" alt="こぐまえんでの生活"><img src="<?php echo get_template_directory_uri(); ?>/shared/images/sp/btn_life.png" class="u-sp" alt="こぐまえんでの生活"></span></a></li>
    <li><a href="<?php echo get_home_url(); ?>/place/" class="green-btn btn-shadow"><span><img src="<?php echo get_template_directory_uri(); ?>/shared/images/btn_flow.png" class="u-pc" alt="周辺環境"><img src="<?php echo get_template_directory_uri(); ?>/shared/images/sp/btn_flow.png" class="u-sp" alt="周辺環境"></span></a></li>
</ul>
<!-- .footer-pagenav // --></div>

<!-- #Main // --></section>
<!-- #Content // --></section>
<!-- △メインコンテンツここまで // -->

<?php get_footer(); ?>

<!-- #Page // --></div>

<?php include( TEMPLATEPATH . '/gr_tag.php' ); ?>

</body>
</html>