<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,shrink-to-fit=no">
<meta name="format-detection" content="telephone=no,address=no,email=no">

<link rel="index" href="<?php echo get_template_directory_uri(); ?>/">
<link rel="shortcut icon" type="image/vnd.microsoft.icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/shared/css/global.css" media="all">
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/shared/css/common.css" media="all">
<?php if ( is_front_page() ) : ?>
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/index.css" media="all">
<?php elseif (is_page('privacy')) : ?>
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/privacy/index.css" media="all">
<?php elseif (is_page('place')) : ?>
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/place/index.css" media="all">
<?php elseif (is_page('life')) : ?>
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/life/index.css" media="all">
<?php elseif (is_page('flow')) : ?>
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/flow/index.css" media="all">
<?php elseif (is_page('feature')) : ?>
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/feature/index.css" media="all">
<?php elseif (is_page('about')) : ?>
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/about/index.css" media="all">
 <script>
      function initMap() {
        // Create a map object and specify the DOM element for display.
        var map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: 34.6870279, lng: 135.180493},
          scrollwheel: false,
          zoom: 17
        });
        var marker = new google.maps.Marker({
          map: map ,
          position: new google.maps.LatLng( 34.6870279 , 135.180493 )
        }) ;
        // var infoWindow = new google.maps.InfoWindow( {
        //   content: "こぐまえん" ,
        //   position: new google.maps.LatLng( 34.6870279 , 135.180493 ),
        // });
        // infoWindow.open( map ) ;
      }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDmICh495-tfDW-XAH0Rt8-_CbV3p8h1ck&callback=initMap" async defer>
</script>
<?php elseif (is_page('concept')) : ?>
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/concept/index.css" media="all">
<?php elseif (is_page('contact')) : ?>
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/contact/index.css" media="all">
<?php elseif (is_page('thanks')) : ?>
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/thanks/index.css" media="all">
<?php elseif (is_page('lunch')) : ?>
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/lunch/index.css" media="all">
<script src="<?php echo get_template_directory_uri(); ?>/shared/js/height.js"></script>
<?php elseif (is_category()) : ?>
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/news/index.css" media="all">
<?php elseif (is_archive()) : ?>
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/news/index.css" media="all">
<?php elseif (is_single('blog')) : ?>
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/blog/index.css" media="all">
<?php elseif (is_single()) : ?>
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/news/index.css" media="all">
<?php endif; ?>
<?php if (is_home()) : ?>
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/news/index.css" media="all">
<?php endif; ?>

<script src="<?php echo get_template_directory_uri(); ?>/shared/js/common.js"></script>

<!--[if lt IE 9]><script src="<?php echo get_template_directory_uri(); ?>/shared/js/html5shiv.js"></script><![endif]-->
