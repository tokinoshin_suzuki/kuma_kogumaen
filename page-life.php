<?php get_header(); ?>
<div id="Page">

<?php include( TEMPLATEPATH . '/head.php' ); ?>

<!-- ▽メインコンテンツここから // -->
<section id="Content" class="g-content">
<div class="pagettl u-pc">
<h1><img src="<?php echo get_template_directory_uri(); ?>/images/life/common/page_ttl.png" width="274" height="30" alt="こぐまえんでの生活"></h1>
<!-- .pagettl // --></div>
<div class="pagettl u-sp">
<h1><img src="<?php echo get_template_directory_uri(); ?>/images/life/common/sp/page_ttl.jpg" width="640" height="180" alt="こぐまえんでの生活"></h1>
<!-- .pagettl // --></div>

<section id="Main">

<p class="header-txt">こぐまえんでは、温かく、のびのびとした家庭的な雰囲気で、夢のある楽しい保育を展開しています。<br>
子どもには健やかに育って欲しいものです。<br>
子どもの成長段階、発達段階に合わせた、子ども一人ひとりに寄り添う保育を展開しております。</p>

<div class="target" id="target-flow"></div>
<div class="secttl">
<h2><img src="<?php echo get_template_directory_uri(); ?>/images/life/index/ttl_section01.png" Width="640" height="42" alt="こぐまえんでの1日の流れ"></h2>
<!-- .secttl // --></div>

<div class="timetable">
<div class="timetable-box">
<div class="timetable-newborn">
<h2 class="timetable-ttl">0才児</h2>
<ul class="item">
<li class="item-gray">
<div class="item-clock">
<img src="<?php echo get_template_directory_uri(); ?>/images/life/index/ico_newborn01.png" width="45" height="80" alt="8:00" class="u-pc">
<img src="<?php echo get_template_directory_uri(); ?>/images/life/index/sp/ico_newborn01.png" alt="8:00" class="u-sp">
<!-- .item-clock // --></div>
<div class="item-txt">
<p>順次登園<br>健康観察・検温<br>自由遊び</p>
<!-- .item-txt // --></div>
</li>
<li>
<div class="item-clock">
<img src="<?php echo get_template_directory_uri(); ?>/images/life/index/ico_newborn02.png" width="45" height="80" alt="9:30" class="u-pc">
<img src="<?php echo get_template_directory_uri(); ?>/images/life/index/sp/ico_newborn02.png" alt="9:30" class="u-sp">
<!-- .item-clock // --></div>
<div class="item-txt">
<p>おやつ<span>（月齢による）</span><br>睡眠・自由遊び<br><span>遊びやベビーマッサージなどで保育者と遊びます。一人ひとりの発達に合わせて睡眠やミルク・食事等の生活リズムを整えていきます。</span></p>
<p><img src="<?php echo get_template_directory_uri(); ?>/images/life/index/img_newborn01.jpg" width="365" height="168" class="u-pc"><img src="<?php echo get_template_directory_uri(); ?>/images/life/index/sp/img_newborn01.jpg" class="u-sp"></p>
<!-- .item-txt // --></div>
</li>
<li class="item-gray">
<div class="item-clock">
<img src="<?php echo get_template_directory_uri(); ?>/images/life/index/ico_newborn03.png" width="45" height="82" alt="10:30" class="u-pc">
<img src="<?php echo get_template_directory_uri(); ?>/images/life/index/sp/ico_newborn03.png" alt="10:30" class="u-sp">
<!-- .item-clock // --></div>
<div class="item-txt">
<p>ミルク</p>
<!-- .item-txt // --></div>
</li>
<li>
<div class="item-clock">
<img src="<?php echo get_template_directory_uri(); ?>/images/life/index/ico_newborn04.png" width="45" height="82" alt="10:40 11:00" class="u-pc">
<img src="<?php echo get_template_directory_uri(); ?>/images/life/index/sp/ico_newborn04.png" alt="10:40 11:00" class="u-sp">
<!-- .item-clock // --></div>
<div class="item-txt">
<p>離乳食</p>
<p><img src="<?php echo get_template_directory_uri(); ?>/images/life/index/img_newborn02.jpg" width="365" height="168" class="u-pc"><img src="<?php echo get_template_directory_uri(); ?>/images/life/index/sp/img_newborn02.jpg" class="u-sp"></p>
<!-- .item-txt // --></div>
</li>
<li class="item-gray">
<div class="item-clock">
<img src="<?php echo get_template_directory_uri(); ?>/images/life/index/ico_newborn05.png" width="45" height="98" alt="12:00 13:00" class="u-pc">
<img src="<?php echo get_template_directory_uri(); ?>/images/life/index/sp/ico_newborn05.png" alt="12:00 13:00" class="u-sp">
<!-- .item-clock // --></div>
<div class="item-txt">
<p>お昼寝</p>
<p><img src="<?php echo get_template_directory_uri(); ?>/images/life/index/img_newborn03.jpg" width="365" height="168" class="u-pc"><img src="<?php echo get_template_directory_uri(); ?>/images/life/index/sp/img_newborn03.jpg" class="u-sp"></p>
<!-- .item-txt // --></div>
</li>
<li>
<div class="item-clock">
<img src="<?php echo get_template_directory_uri(); ?>/images/life/index/ico_newborn06.png" width="45" height="74" alt="15:00" class="u-pc">
<img src="<?php echo get_template_directory_uri(); ?>/images/life/index/sp/ico_newborn06.png" alt="15:00" class="u-sp">
<!-- .item-clock // --></div>
<div class="item-txt">
<p>めざめ、おやつ<br>ミルク、検温</p>
<!-- .item-txt // --></div>
</li>
<li class="item-gray">
<div class="item-clock">
<img src="<?php echo get_template_directory_uri(); ?>/images/life/index/ico_newborn07.png" width="45" height="81" alt="16:00" class="u-pc">
<img src="<?php echo get_template_directory_uri(); ?>/images/life/index/sp/ico_newborn07.png" alt="16:00" class="u-sp">
<!-- .item-clock // --></div>
<div class="item-txt">
<p>睡眠・自由遊び</p>
<!-- .item-txt // --></div>
</li>
<li>
<div class="item-clock">
<img src="<?php echo get_template_directory_uri(); ?>/images/life/index/ico_newborn08.png" width="45" height="104" alt="17:30" class="u-pc">
<img src="<?php echo get_template_directory_uri(); ?>/images/life/index/sp/ico_newborn08.png" alt="17:30" class="u-sp">
<!-- .item-clock // --></div>
<div class="item-txt">
<p>帰りのお集まり</p>
<!-- .item-txt // --></div>
</li>
<li class="item-gray">
<div class="item-clock">
<img src="<?php echo get_template_directory_uri(); ?>/images/life/index/ico_newborn09.png" width="45" height="95" alt="18:00" class="u-pc">
<img src="<?php echo get_template_directory_uri(); ?>/images/life/index/sp/ico_newborn09.png" alt="18:00" class="u-sp">
<!-- .item-clock // --></div>
<div class="item-txt">
<p>保育終了</p>
<!-- .item-txt // --></div>
</li>
</ul>
<!-- .timetable-newborn // --></div>

<div class="timetable-infant">
<h2 class="timetable-ttl">1〜3才児</h2>
<ul class="item">
<li class="item-gray">
<div class="item-clock">
<img src="<?php echo get_template_directory_uri(); ?>/images/life/index/ico_infant01.png" width="45" height="80" alt="8:00" class="u-pc">
<img src="<?php echo get_template_directory_uri(); ?>/images/life/index/sp/ico_infant01.png" alt="8:00" class="u-sp">
<!-- .item-clock // --></div>
<div class="item-txt">
<p>順次登園<br>健康観察・検温<br>自由遊び</p>
<!-- .item-txt // --></div>
</li>
<li>
<div class="item-clock">
<img src="<?php echo get_template_directory_uri(); ?>/images/life/index/ico_infant02.png" width="45" height="81" alt="9:30" class="u-pc">
<img src="<?php echo get_template_directory_uri(); ?>/images/life/index/sp/ico_infant02.png" alt="9:30" class="u-sp">
<!-- .item-clock // --></div>
<div class="item-txt">
<p>おやつ<span>（月齢による）</span><br>睡眠・自由遊び<br><span>徐々に歩行が安定してきて活動範囲も広がります。体操したり季節の歌をうたったり、友達とのかかわりを増やしながら遊びます。</span></p>
<p><img src="<?php echo get_template_directory_uri(); ?>/images/life/index/img_infant01.jpg" width="365" height="168" class="u-pc"><img src="<?php echo get_template_directory_uri(); ?>/images/life/index/sp/img_infant01.jpg" class="u-sp"></p>
<!-- .item-txt // --></div>
</li>
<li class="item-gray">
<div class="item-clock">
<img src="<?php echo get_template_directory_uri(); ?>/images/life/index/ico_infant03.png" width="45" height="82" alt="12:00" class="u-pc">
<img src="<?php echo get_template_directory_uri(); ?>/images/life/index/sp/ico_infant03.png" alt="12:00" class="u-sp">
<!-- .item-clock // --></div>
<div class="item-txt">
<p>食事</p>
<p><img src="<?php echo get_template_directory_uri(); ?>/images/life/index/img_infant02.jpg" width="365" height="168" class="u-pc"><img src="<?php echo get_template_directory_uri(); ?>/images/life/index/sp/img_infant02.jpg" class="u-sp"></p>
<!-- .item-txt // --></div>
</li>
<li class="item-gray u-pc"></li>
<li>
<div class="item-clock">
<img src="<?php echo get_template_directory_uri(); ?>/images/life/index/ico_infant04.png" width="45" height="79" alt="13:00" class="u-pc">
<img src="<?php echo get_template_directory_uri(); ?>/images/life/index/sp/ico_infant04.png" alt="13:00" class="u-sp">
<!-- .item-clock // --></div>
<div class="item-txt">
<p>お昼寝</p>
<p><img src="<?php echo get_template_directory_uri(); ?>/images/life/index/img_infant03.jpg" width="365" height="168" class="u-pc"><img src="<?php echo get_template_directory_uri(); ?>/images/life/index/sp/img_infant03.jpg" class="u-sp"></p>
<!-- .item-txt // --></div>
</li>
<li class="item-gray">
<div class="item-clock">
<img src="<?php echo get_template_directory_uri(); ?>/images/life/index/ico_infant05.png" width="45" height="74" alt="15:00" class="u-pc">
<img src="<?php echo get_template_directory_uri(); ?>/images/life/index/sp/ico_infant05.png" alt="15:00" class="u-sp">
<!-- .item-clock // --></div>
<div class="item-txt">
<p>めざめ、おやつ<br>検温</p>
<!-- .item-txt // --></div>
</li>
<li>
<div class="item-clock">
<img src="<?php echo get_template_directory_uri(); ?>/images/life/index/ico_infant06.png" width="45" height="81" alt="16:00" class="u-pc">
<img src="<?php echo get_template_directory_uri(); ?>/images/life/index/sp/ico_infant06.png" alt="16:00" class="u-sp">
<!-- .item-clock // --></div>
<div class="item-txt">
<p>睡眠・自由遊び</p>
<!-- .item-txt // --></div>
</li>
<li class="item-gray">
<div class="item-clock">
<img src="<?php echo get_template_directory_uri(); ?>/images/life/index/ico_infant07.png" width="45" height="82" alt="17:30" class="u-pc">
<img src="<?php echo get_template_directory_uri(); ?>/images/life/index/sp/ico_infant07.png" alt="17:30" class="u-sp">
<!-- .item-clock // --></div>
<div class="item-txt">
<p>帰りのお集まり</p>
<!-- .item-txt // --></div>
</li>
<li>
<div class="item-clock">
<img src="<?php echo get_template_directory_uri(); ?>/images/life/index/ico_infant08.png" width="45" height="95" alt="18:00" class="u-pc">
<img src="<?php echo get_template_directory_uri(); ?>/images/life/index/sp/ico_infant08.png" alt="18:00" class="u-sp">
<!-- .item-clock // --></div>
<div class="item-txt">
<p>保育終了</p>
<!-- .item-txt // --></div>
</li>
</ul>
<!-- .timetable-infant // --></div>
<!-- .timetable-box // --></div>
<p class="timetable-txt">※上記表はデイリープログラムの一例です。<br>※お子様一人ひとりに合わせた保育を実施いたします。</p>
<!-- .timetable // --></div>

<div class="target" id="target-oneyear"></div>
<div class="secttl">
<h2><img src="<?php echo get_template_directory_uri(); ?>/images/life/index/ttl_section02.png" Width="533" height="42" alt="こぐまえんでの1年"></h2>
<p class="secttl-txt">年間イベント・行事</p>
<!-- .secttl // --></div>

<div class="schedule">
<div class="schedule-season">
<img src="<?php echo get_template_directory_uri(); ?>/images/life/index/img_schedule01.jpg">
<h3>4月〜6月</h3>
<ul>
<li>進級・入園おめでとうの会</li>
<li>親子感謝の日</li>
<li>虫歯予防デー</li>
</ul>
<!-- .schedule-season // --></div>
<div class="schedule-season">
<img src="<?php echo get_template_directory_uri(); ?>/images/life/index/img_schedule02.jpg">
<h3>7月〜9月</h3>
<ul>
<li>たなばた会</li>
<li>水遊び</li>
<li>お月見会</li>
</ul>
<!-- .schedule-season // --></div>
<div class="schedule-season">
<img src="<?php echo get_template_directory_uri(); ?>/images/life/index/img_schedule03.jpg">
<h3>10月〜12月</h3>
<ul>
<li>秋の自然体験</li>
<li>読書の秋、読み聞かせ</li>
<li>クリスマス会</li>
</ul>
<!-- .schedule-season // --></div>
<div class="schedule-season">
<img src="<?php echo get_template_directory_uri(); ?>/images/life/index/img_schedule04.jpg">
<h3>1月〜3月</h3>
<ul>
<li>お正月遊び、豆まき</li>
<li>ひな祭り、お別れ会</li>
<li>健康診断</li>
</ul>
<!-- .schedule-season // --></div>
<!-- .schedule // --></div>

<div class="footer-nav">
<ul>
	<li><a href="<?php echo get_home_url(); ?>/concept/" class="blue-btn btn-shadow"><span><img src="<?php echo get_template_directory_uri(); ?>/shared/images/btn_concept.png" class="u-pc" alt="保育理念"><img src="<?php echo get_template_directory_uri(); ?>/shared/images/sp/btn_concept.png" class="u-sp" alt="保育理念"></span></a></li>
    <li><a href="<?php echo get_home_url(); ?>/place/" class="green-btn btn-shadow"><span><img src="<?php echo get_template_directory_uri(); ?>/shared/images/btn_flow.png" class="u-pc" alt="周辺環境"><img src="<?php echo get_template_directory_uri(); ?>/shared/images/sp/btn_flow.png" class="u-sp" alt="周辺環境"></span></a></li>
    <li><a href="<?php echo get_home_url(); ?>/about/" class="brown-btn btn-shadow"><span><img src="<?php echo get_template_directory_uri(); ?>/shared/images/btn_place.png" class="u-pc" alt="こぐまえん概要"><img src="<?php echo get_template_directory_uri(); ?>/shared/images/sp/btn_place.png" class="u-sp" alt="こぐまえん概要"></span></a></li>
</ul>
<!-- .footer-pagenav // --></div>

<!-- #Main // --></section>
<!-- #Content // --></section>
<!-- △メインコンテンツここまで // -->

<?php get_footer(); ?>

<!-- #Page // --></div>

<?php include( TEMPLATEPATH . '/gr_tag.php' ); ?>

</body>
</html>
