<?php get_header(); ?>
<div id="Page">

<?php include( TEMPLATEPATH . '/head.php' ); ?>

<!-- ▽メインコンテンツここから // -->
<section id="Content" class="g-content">
<div class="pagettl u-pc">
<h1><img src="<?php echo get_template_directory_uri(); ?>/images/feature/common/page_ttl.png" width="244" height="30" alt="こぐまえんの特徴"></h1>
<!-- .pagettl // --></div>
<div class="pagettl u-sp">
<h1><img src="<?php echo get_template_directory_uri(); ?>/images/feature/common/sp/page_ttl.jpg" width="640" height="180" alt="こぐまえんの特徴"></h1>
<!-- .pagettl // --></div>
<section id="Main">
<div class="header-pagenav">
<ul>
<li><a href="#target-about" class="btn-pagenav btn-shadow">内閣府認可保育園について</a></li>
<li><a href="#target-building" class="btn-pagenav btn-shadow">園舎のこだわり</a></li>
<li><a href="#target-environment" class="btn-pagenav btn-shadow">整った知育環境</a></li>
<li><a href="#target-security" class="btn-pagenav btn-shadow">徹底した安全対策</a></li>
</ul>
<!-- .pagenavi // --></div>
<p class="header-img u-pc"><img src="<?php echo get_template_directory_uri(); ?>/images/feature/index/img_header.png" width="952" height="220" alt="「こぐまえん」は医療法人神甲会 隈病院が監督する内閣府認可保育園です。"></p>
<p class="header-img u-sp"><img src="<?php echo get_template_directory_uri(); ?>/images/feature/index/sp/img_header.jpg" width="640" height="217" alt="「こぐまえん」は医療法人神甲会 隈病院が監督する内閣府認可保育園です。"></p>

<div class="target" id="target-about"></div>
<div class="secttl ttl-about">
  <h2><img class="u-pc" src="<?php echo get_template_directory_uri(); ?>/images/feature/index/ttl_section01.png" Width="656" height="42" alt="内閣府認可保育園について"><img class="u-sp" src="<?php echo get_template_directory_uri(); ?>/images/feature/index/sp/ttl_section01.png" Width="616" height="42" alt="内閣府認可保育園について"></h2>

  <p class="secttl-txt">待機児童の問題を解決するための政府の新制度（企業主導型保育事業）により、<br class="u-pc">
認可保育と同等の基準をクリアした設備や環境で、内閣府からの支援を得ている保育施設です。</p>
<!-- .secttl // --></div>
<div class="secbox about">
<p class="about-txt">一億総活躍社会「夢をつむぐ子育て支援（第二の矢）」の実現に向けて、子ども・子育て支援法の一部が改正されました。その中で待機児童の解消を目指すために設けられたのが「企業主導型保育事業」です。企業が社員や地域の子どもを対象として事業所内や近接地に設置する保育施設のことで内閣府が支援するものです。</p>
<div class="itembox aboutbox cf">
<div class="item">
<img class="img-about u-pc" src="<?php echo get_template_directory_uri(); ?>/images/feature/index/img_about01.png" Width="135" height="80" alt="POINT 1">
<img class="img-about u-sp" src="<?php echo get_template_directory_uri(); ?>/images/feature/index/sp/img_about01.png" Width="180" height="106" alt="POINT 1">
<p class="item-ttl">認可保育と同等の<br>設備基準をクリアしている</p>
<p class="item-txt">内閣府の認可を受けるため、認可外の保育園とは異なった基準が設けられています。職員数や職員の資格割合、設備等の基準は自治体認可の小規模保育事業や事業所内保育事業と同様です。人員・設備とも自治体認可並みの質を確保しています。</p>
<!-- .item // --></div>
<div class="item">
<img class="img-about u-pc" src="<?php echo get_template_directory_uri(); ?>/images/feature/index/img_about02.png" Width="130" height="78" alt="POINT 2">
<img class="img-about u-sp" src="<?php echo get_template_directory_uri(); ?>/images/feature/index/sp/img_about02.png" Width="173" height="103" alt="POINT 2">
<p class="item-ttl">認可保育園の費用と<br>同等の保育料で利用</p>
<p class="item-txt">内閣府から認可保育園と同等の助成金を受けているため、保育料も認可保育園と同等の料金で利用できます。収入要件等によっては認可保育園よりも安い料金で利用することができます。</p>
<!-- .item // --></div>
<div class="item">
<img class="img-about u-pc" src="<?php echo get_template_directory_uri(); ?>/images/feature/index/img_about03.png" Width="134" height="74" alt="POINT 3">
<img class="img-about u-sp" src="<?php echo get_template_directory_uri(); ?>/images/feature/index/sp/img_about03.png" Width="180" height="93" alt="POINT 3">
<p class="item-ttl">園児に対して<br>柔軟な保育サービス</p>
<p class="item-txt">事業所内保育園のため、多様な勤務パターンなど、働き方に応じて柔軟な保育サービスが提供でき、仕事との両立をサポートします。そして小規模な事業所内保育所だからこそできる、大規模な集団保育では難しいといわれている、一人ひとりの園児の発達特性に応じた質の高いきめ細やかで柔軟な保育サービスを行います。</p>
<!-- .item // --></div>
<!-- .itembox .aboutbox // --></div>
<!-- .secbox .about // --></div>

<div class="target" id="target-building"></div>
<div class="secttl ttl-building">
<h2><img src="<?php echo get_template_directory_uri(); ?>/images/feature/index/ttl_section02.png" Width="505" height="42" alt="園舎へのこだわり"></h2>
<p class="secttl-txt">こぐまえんの設計・デザインは医療施設である隈病院を建てた担当者が関わっております。<br>隈病院独自の理念に基づいて利用者様のことを考えて設立した保育園だからです。</p>
<!-- .secttl // --></div>
<div class="secbox building">
<div class="itembox buildingbox">
<div class="item img-design u-pc">
<img src="<?php echo get_template_directory_uri(); ?>/images/feature/index/img_building01.png" width="443" height="484" alt="">
<!-- .item // --></div>
<div class="item txt-design">
<h3 class="item-subttl">子たちがのびのびと過ごせる<br>園舎デザイン</h3>
<p class="item-txt">子どもにとって自宅やご両親様から離れて過ごす時間は子どもにとって不安になってしまうものです。そんな子どもたちのために不安を減らし楽しく、健やかに過ごせるように隈病院が設立した保育園です。おなじく家庭環境以外に預けるのは親御さんもとっても心配になるものです。<br>
患者様のことを思って建てた隈病院と同じ設計・デザイナが一丸となり、子どものことを考え子どもにとっての危険を限りなく無くした園舎となっています。<br>
子どもの体調のことを考えた安静室や子どもが広く感じる吹き抜けなど、子どものことを第一優先に考えた設計・デザインとなっています。</p>
<!-- .item // --></div>
<div class="item img-design u-sp">
<img src="<?php echo get_template_directory_uri(); ?>/images/feature/index/img_building01.png" width="443" height="484" alt="">
<!-- .item // --></div>
<!-- .itembox buildingbox // --></div>

<div class="itembox buildingbox">
<div class="item txt-plan">
<h3 class="item-subttl">子どもの目線に<br class="u-pc">合わせた設計</h3>
<p class="item-txt">子どもの背丈に合わせた拭き取れる壁紙や<br class="u-pc">
元気に遊ぶ子どもが角にぶつかっても<br class="u-pc">
大丈夫なように角を丸くしています。</p>
<!-- .item // --></div>
<div class="item img-plan u-pc">
<img src="<?php echo get_template_directory_uri(); ?>/images/feature/index/img_building02.png" width="954" height="560" alt="">
<!-- .item // --></div>
<div class="item img-plan u-sp">
<img src="<?php echo get_template_directory_uri(); ?>/images/feature/index/sp/img_building.png" width="504" height="434" alt="">
<!-- .item // --></div>
<!-- .itembox buildingbox // --></div>
<!-- .secbox .building // --></div>

<div class="target" id="target-environment"></div>
<div class="secttl ttl-environment">
<h2><img src="<?php echo get_template_directory_uri(); ?>/images/feature/index/ttl_section03.png" Width="479" height="42" alt="整った知育環境"></h2>
<p class="secttl-txt">お子様のよりよい成長のために、<br class="u-sp">細部にこだわって環境作りをしています。</p>
<!-- .secttl // --></div>
<div class="secbox environment">
<div class="itembox environmentbox">
<div class="item img-temperature u-pc">
<img src="<?php echo get_template_directory_uri(); ?>/images/feature/index/img_environment01.png" width="581" height="394" alt="">
<!-- .item // --></div>
<div class="item img-temperature u-sp">
<img src="<?php echo get_template_directory_uri(); ?>/images/feature/index/sp/img_environment01.png" width="404" height="328" alt="">
<!-- .item // --></div>
<div class="item txt-temperature">
<h3 class="item-subttl">快適な環境を作る空調と<br>冬でも裸足で遊べる床暖房</h3>
<p class="item-txt">熱交換型換気扇を完備し、きれいな外気を室温と同じに温度まで温めて室内に入れるので室内の温度変化がなく、きれいな空気へと換気します。きれいな空気を室温を変えることなく換気ができるので常に快適な環境となっています。</p>
<!-- .item // --></div>
<!-- .itembox // --></div>

<div class="itembox environmentbox">
<div class="item img-play u-sp">
<img src="<?php echo get_template_directory_uri(); ?>/images/feature/index/sp/img_environment02.png" width="409" height="340" alt="">
<!-- .item // --></div>
<div class="item txt-play">
<h3 class="item-subttl">太陽と一緒に遊び、<br>音で心を育む</h3>
<p class="item-txt">保育室だけではなくバルコニーを完備しているので、風通しのいい場所で日差しを浴びながら楽しく遊べます。沐浴や砂遊びなど子どもにとって楽しい遊びもできます。テラスには洗い場もついているので汚れても安心です。</p>
<!-- .item // --></div>
<div class="item img-play u-pc">
<img src="<?php echo get_template_directory_uri(); ?>/images/feature/index/img_environment02.png" width="632" height="383" alt="">
<!-- .item // --></div>
<!-- .itembox // --></div>

<div class="itembox environmentbox">
<div class="item img-room u-pc">
<img src="<?php echo get_template_directory_uri(); ?>/images/feature/index/img_environment03.png" width="543" height="405" alt="">
<!-- .item // --></div>
<div class="item img-room u-sp">
<img src="<?php echo get_template_directory_uri(); ?>/images/feature/index/sp/img_environment03.png" width="402" height="330" alt="">
<!-- .item // --></div>

<div class="item txt-room">
<h3 class="item-subttl">子どものことを考えた玄関と<br>体調のことを考えた安静室</h3>
<p class="item-txt">ゆるやかなスロープになっている玄関は子どもたちが自分で靴を履いたり脱いだりし易いように広く設けています。下駄箱にはお名前とマークがついていて子ども自身がわかりやすいようにしています。<br>また、子どもは急に体調が変化してしまいます。そのためこぐまえんでは安静室を設定しております。安静室には保育室との間に扉を設置し、静かな環境で安心して休むことができます。</p>
<!-- .item // --></div>
<!-- .itembox // --></div>
<!-- .secbox .environment // --></div>

<div class="target" id="target-security"></div>
<div class="secttl ttl-security">
  <h2><img src="<?php echo get_template_directory_uri(); ?>/images/feature/index/ttl_section04.png" Width="513" height="41" alt="徹底した安全対策"></h2>
  <p class="secttl-txt">充実した防犯対策と防災対策</p>
<!-- .secttl // --></div>
<div class="secbox security">
<div class="security-txt">こぐまえんは隈病院の近接地で大通りから少し外れた落ち着いた場所に建っています。<br class="u-pc">たくさんの窓はまるで星のような配置。柔らかい光が差し込みます。<br class="u-pc">家庭的で暖かい雰囲気の中、子どもが安心して過ごせるよう配慮した園舎です。<br class="u-pc">そんなこぐまえんはセキュリティ面、子どもの環境面にも配慮した園舎です。
<p class="security-img u-pc"><img src="<?php echo get_template_directory_uri(); ?>/images/feature/index/img_security01.png" width="269" height="268" alt=""></p>
<!-- .security-txt // --> </div>

<div class="itembox securitybox">
<div class="item img-disaster u-pc">
<img src="<?php echo get_template_directory_uri(); ?>/images/feature/index/img_security02.png" width="285" height="286" alt="">
<!-- .item // --></div>
<div class="item img-disaster u-sp">
<img src="<?php echo get_template_directory_uri(); ?>/images/feature/index/sp/img_security01.png" width="326" height="330" alt="">
<!-- .item // --></div>
<div class="item txt-disaster">
<h3 class="item-subttl">防災対策</h3>
<p class="item-txt">消防署と共に避難経路を入念に検討し万が一のことがあっても大丈夫なように職員の避難訓練も行っております。また、隈病院の防災センターとも連携しており、迅速に子どもの安全を確保するよう防災対策を整えています。消化器はもちろんのこと退避バルコニー等の防災設備を備えています。</p>
<!-- .item // --></div>
<!-- .itembox // --></div>

<div class="itembox securitybox">
<div class="item img-crime u-pc">
<img src="<?php echo get_template_directory_uri(); ?>/images/feature/index/img_security03.png" width="295" height="282" alt="">
<!-- .item // --></div>
<div class="item img-crime u-sp">
<img src="<?php echo get_template_directory_uri(); ?>/images/feature/index/sp/img_security02.png" width="340" height="325" alt="">
<!-- .item // --></div>
<div class="item txt-crime">
<h3 class="item-subttl">防犯対策</h3>
<p class="item-txt">通用門を常時施錠し、セキュリティカメラで園舎内外を監視し、解鍵する際はモニター確認の上解錠することで無関係者の侵入を防ぎます。さらにALSOKによる24時間警備システムで子ども達が安心して生活できるよう二重三重に防犯対策を講じています。</p>
<!-- .item // --></div>
<!-- .itembox // --></div>
<!-- .secbox .security // --></div>

<div class="footer-nav">
<ul>
    <li><a href="<?php echo get_home_url(); ?>/life/" class="red-btn btn-shadow"><span><img src="<?php echo get_template_directory_uri(); ?>//shared/images/btn_life.png" class="u-pc" alt="こぐまえんでの生活"><img src="<?php echo get_template_directory_uri(); ?>//shared/images/sp/btn_life.png" class="u-sp" alt="こぐまえんでの生活"></span></a></li>
  <li><a href="<?php echo get_home_url(); ?>/place/" class="green-btn btn-shadow"><span><img src="<?php echo get_template_directory_uri(); ?>//shared/images/btn_flow.png" class="u-pc" alt="周辺環境"><img src="<?php echo get_template_directory_uri(); ?>//shared/images/sp/btn_flow.png" class="u-sp" alt="周辺環境"></span></a></li>
    <li><a href="<?php echo get_home_url(); ?>/about/" class="brown-btn btn-shadow"><span><img src="<?php echo get_template_directory_uri(); ?>//shared/images/btn_place.png" class="u-pc" alt="こぐまえん概要"><img src="<?php echo get_template_directory_uri(); ?>//shared/images/sp/btn_place.png" class="u-sp" alt="こぐまえん概要"></span></a></li>
</ul>
<!-- .footer-pagenav // --></div>


<!-- #Main // --></section>
<!-- #Content // --></section>
<!-- △メインコンテンツここまで // -->

<?php get_footer(); ?>

<!-- #Page // --></div>

<?php include( TEMPLATEPATH . '/gr_tag.php' ); ?>


</body>
</html>
