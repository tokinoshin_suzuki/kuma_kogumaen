<?php
/*
Template Name: プライバシーポリシー
*/
?>
<?php get_header(); ?>
<div id="Page">

<?php include( TEMPLATEPATH . '/head.php' ); ?>

<!-- ▽メインコンテンツここから // -->
<section id="Content" class="g-content">
<div class="pagettl u-pc">
<h1><img src="<?php echo get_template_directory_uri(); ?>/images/privacy/common/page_ttl.png" width="290" height="30" alt="プライバシーポリシー"></h1>
<!-- .pagettl // --></div>
<div class="pagettl u-sp">
<h1><img src="<?php echo get_template_directory_uri(); ?>/images/privacy/common/sp/page_ttl.jpg" width="640" height="180" alt="プライバシーポリシー"></h1>
<!-- .pagettl // --></div>
<section id="Main">
<div class="secttl ttl-about">
  <h2><img src="<?php echo get_template_directory_uri(); ?>/images/privacy/index/ttl_section.png" Width="656" height="41" alt="個人情報の取り扱いについて"></h2>
<!-- .secttl // --></div>
<div class="secbox">
<div class="item">
<p class="item-ttl">取得及び利用目的について</p>
<p class="item-txt">当園に入園される園児および保護者の氏名、年齢、住所、連絡先(電話番号、メールアドレス等)の個人を認識できる情報や、保護者の勤務先、園児の出生後の発達の状態(母子手帳、健康診断書等)に関する情報を取得、保護者への配付などの為の写真撮影を行います。
取得した個人情報は、入園申込の手続き、緊急連絡、園児の健康管理、園内の掲示や園だより、ホームページ掲載、保育サービスの運営管理業務などに利用し、それ以外での目的での利用はいたしません。</p>
<!-- .item // --></div>

<div class="item">
<p class="item-ttl">預託・提供について</p>
<p class="item-txt">当園は、法令で定める場合(所轄官庁への運営状況の報告)、警察の捜査協力、園児の生命や身体にかかわる緊急・重大な場合等を除き、上記で取得した情報を利用目的の範囲を超えて、その情報を提供していただいた個人情報を第三者への提供や預託はいたしません。</p>
<!-- .item // --></div>

<div class="item">
<p class="item-ttl">情報の開示・訂正・削除について</p>
<p class="item-txt">当園で管理する個人データについて、お問い合わせいただいた際、当該個人情報の所有者であるご本人様からのお問い合わせであることを確認した上で、当園の運営管理に支障をきたさない合理的な範囲内で、保有している個人データについてのお問い合わせに応じるものとします。また、個人情報の開示のご要望につきましては、文書にて対応するものとします。</p>
<!-- .item // --></div>

<div class="item">
<p class="item-ttl">個人情報の管理について</p>
<p class="item-txt">入園申込やこれに附属する申込手続きの為にご提供いただいた個人情報および、当園を利用するにあたり、継続的に取得する個人情報(保護者と職員の連絡情報を含む)につきましては、当園にて安全管理いたします。卒園・転園をされたお子さまの個人情報については、法令に定める期間を保管した後、当園で責任を持って廃棄いたします。</p>
<!-- .item // --></div>

<div class="item">
<p class="item-ttl">個人情報の正確性の確保について</p>
<p class="item-txt">当園は、利用目的の達成に必要な範囲で、保有する個人情報を正確かつ最新の内容に保つよう努力します。住所、氏名、電話番号等、お届けされている情報に変更があった場合、また訂正が必要な場合は、速やかに当園までお申し出ください。</p>
<!-- .item // --></div>
<!-- .secbox // --></div>


<!-- #Main // --></section>
<!-- #Content // --></section>
<!-- △メインコンテンツここまで // -->

<?php get_footer(); ?>

<!-- #Page // --></div>

<?php include( TEMPLATEPATH . '/gr_tag.php' ); ?>


</body>
</html>