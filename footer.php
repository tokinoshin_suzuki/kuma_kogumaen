<footer id="GlobalFooter">
<div class="inner">
<div class="footer-items-top u-pc cf">
<div class="btn-hidden u-pc"><a href="<?php echo get_home_url(); ?>/contact/">
<img src="<?php echo get_template_directory_uri(); ?>/shared/images/btn_contact.png" width="200" height ="230" alt="お問い合わせ・資料請求"></a>
<!-- .btn-hidden // --></div>
<p class="footer-item-top item-logo"><img class="footer-img-top" src="<?php echo get_template_directory_uri(); ?>/shared/images/logo_footer.png" alt="こぐまえん"></p>
<p class="footer-item-top item-contact"><img class="footer-img-top" src="<?php echo get_template_directory_uri(); ?>/shared/images/img_footer_tel.png" alt="お電話でのご相談もお受けしております。下記までお気軽にお問合せください。078-381-5824 受付時間9：00〜17：00"></p>
<p class="footer-item-top item-btn"><a class="btn-contact btn-shadow" href="<?php echo get_home_url(); ?>/contact/"><span>問い合わせ・資料請求はこちら</span></a></p>
<!-- .footer-items-top // --></div>
<div class="footer-items-top u-sp">
<p class="footer-item-top item-logo"><img class="footer-img-top" src="<?php echo get_template_directory_uri(); ?>/shared/images/sp/logo_footer.png" width="224" height="88"  alt="こぐまえん"></p>
<p class="footer-item-top item-txt">お電話でのご相談もお受けしております。<br>下記までお気軽にお問合せください。</p>
<p class="footer-item-top item-contact"><a href="tel:078-381-5824"><img class="footer-img-top" src="<?php echo get_template_directory_uri(); ?>/shared/images/sp/img_footer_tel.png"  width="427" height ="80" alt="078-381-5824 受付時間9：00〜17：00"></a></p>
<p class="footer-item-top"><a class="btn-contact btn-shadow" href="<?php echo get_home_url(); ?>/contact/"><span>問い合わせ・資料請求はこちら</span></a></p>
<div class="fb-like-box contact" data-href="https://www.facebook.com/こぐまえん-214030792372617/" data-width="300px" data-height="240px" data-colorscheme="light" data-show-faces="true" data-header="true" data-stream="false" data-show-border="true"></div>
<!-- .footer-items-top // --></div>

<div class="footer-items-bottom u-pc cf">
<ul class="footer-naviList">
<li class="footer-naviList-item"><a href="<?php echo get_home_url(); ?>/concept/"><span>保育理念</span></a></li>
<li class="footer-naviList-item"><a href="<?php echo get_home_url(); ?>/feature/"><span>こぐまえんの特徴</span></a></li>
<li>
<ul class="footer-naviList-smallClass">
<li class="footer-naviList-item"><a href="<?php echo get_home_url(); ?>/feature/?id=target-about"><span>内閣府認可保育園について</span></a></li>
<li class="footer-naviList-item"><a href="<?php echo get_home_url(); ?>/feature/?id=target-building"><span>園舎のこだわり</span></a></li>
<li class="footer-naviList-item"><a href="<?php echo get_home_url(); ?>/feature/?id=target-environment"><span>整った知育環境</span></a></li>
<li class="footer-naviList-item"><a href="<?php echo get_home_url(); ?>/feature/?id=target-security"><span>徹底した安全対策</span></a></li>
</ul>
</li>
<!-- .footer-naviList // --></ul>
<ul class="footer-naviList">
<li class="footer-naviList-item"><a href="<?php echo get_home_url(); ?>/life/"><span>こぐまえんでの生活</span></a></li>
<li>
<ul class="footer-naviList-smallClass">
<li class="footer-naviList-item"><a href="<?php echo get_home_url(); ?>/life/?id=target-flow"><span>こぐまえんでの1日の流れ</span></a></li>
<li class="footer-naviList-item"><a href="<?php echo get_home_url(); ?>/life/?id=target-oneyear"><span>こぐまえんでの1年</span></a></li>
</ul>
</li>
<li class="footer-naviList-item"><a href="<?php echo get_home_url(); ?>/place/"><span>周辺環境</span></a></li>
<!-- .footer-naviList // --></ul>
<ul class="footer-naviList">
<li class="footer-naviList-item"><a href="<?php echo get_home_url(); ?>/flow/"><span>入園案内</span></a></li>
<!-- <li>
<ul class="footer-naviList-smallClass">
<li class="footer-naviList-item"><a href=""><span>募集</span></a></li>
<li class="footer-naviList-item"><a href=""><span>入園の流れ</span></a></li>
</ul> -->
</li>
<li class="footer-naviList-item"><a href="<?php echo get_home_url(); ?>/about/"><span>こぐまえん概要</span></a></li>
<li class="footer-naviList-item"><a href="<?php echo get_home_url(); ?>/privacy/"><span>プライバシーポリシー</span></a></li>
<!-- .footer-naviList // --></ul>
<div class="fb-like-box contact" data-href="https://www.facebook.com/こぐまえん-214030792372617/" data-width="306" data-height="345" data-colorscheme="light" data-show-faces="true" data-header="true" data-stream="false" data-show-border="true"></div>
<!-- .footer-items-bottom // --></div>
<!-- .inner // --></div>
<p class="copy"><span>&copy;</span> Kogumaen all rights reserved.</p>
<!-- #GlobalFooter // --></footer>
<!-- ▽footer -->
<?php wp_footer(); ?>
<!-- ▲footer -->