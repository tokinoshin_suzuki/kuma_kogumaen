<?php get_header(); ?>
<div id="Page">

<?php include( TEMPLATEPATH . '/head.php' ); ?>
<!-- ▽メインコンテンツここから // -->
<section id="Content" class="g-content">
<div class="pagettl u-pc">
<h1><img src="<?php echo get_template_directory_uri(); ?>/images/place/common/page_ttl.png" width="123" height="30" alt="周辺環境"></h1>
<!-- .pagettl // --></div>
<div class="pagettl u-sp">
<h1><img src="<?php echo get_template_directory_uri(); ?>/images/place/common/sp/page_ttl.jpg" width="640" height="180" alt="周辺環境"></h1>
<!-- .pagettl // --></div>
<section id="Main">
<div class="secttl">
<h2><img src="<?php echo get_template_directory_uri(); ?>/images/place/index/ttl_section.png" Width="582" height="42" alt="こぐまえんの周辺環境"></h2>
<ul class="secttl-list">
<li class="secttl-link"><a href="#target-station">駅</a></li>
<li class="secttl-link"><a href="#target-hospital">病院</a></li>
<li class="secttl-link"><a href="#target-park">公園</a></li>
<li class="secttl-link"><a href="#target-kindergerten">保育園・幼稚園・児童館</a></li>
</ul>
<!-- .secttl // --></div>
<div class="target" id="target-station"></div>
<div class="secbox">
<div class="item box-imgleft station">
<div class="item-img">
<img class="u-pc" src="<?php echo get_template_directory_uri(); ?>/images/place/index/img_cont_01.png" width="778" height="479" alt="">
<img class="u-sp" src="<?php echo get_template_directory_uri(); ?>/images/place/index/sp/img_cont_01.png" width="535" height="432" alt="">
<!-- .item-img // --></div>
<div class="item-txts">
<div class="item-txts-inner">
<p class="item-subttl">駅</p>
<ul class="item-list">
<li>高速神戸線「花隈駅」・・・・・・徒歩3分</li>
<li>神戸高速線「西元町駅」・・・・・徒歩5分</li>
<li>地下鉄山手線「大倉山駅」・・・・・・徒歩8分</li>
<li>地下鉄山手線「県庁前駅」・・・・・・徒歩10分</li>
<li>JR・阪神「元町駅」・・・・・・徒歩15分</li>
</ul>
<!-- .item-txts-inner // --></div>
<!-- .item-txts // --></div>
<!-- .item // --></div>

<div class="item box-imgright hospital" id="target-hospital">
<div class="item-img">
<img class="u-pc" src="<?php echo get_template_directory_uri(); ?>/images/place/index/img_cont_02.png" width="800" height="494" alt="">
<img class="u-sp" src="<?php echo get_template_directory_uri(); ?>/images/place/index/sp/img_cont_02.png" width="525" height="445" alt="">
<!-- .item-img // --></div>
<div class="item-txts">
<div class="item-txts-inner">
<p class="item-subttl">パルモア病院<span class="duration">・・・・・・徒歩11分</span></p>
<p class="item-txt">かかりつけ医<br>
  <a href="http://www.palmore.or.jp">http://www.palmore.or.jp</a> </p>
<!-- .item-txts-inner // --></div>
<!-- .item-txts // --></div>
<!-- .item // --></div>

<div class="item box-imgleft park" id="target-park">
<div class="item-img">
<img class="u-pc" src="<?php echo get_template_directory_uri(); ?>/images/place/index/img_cont_03.png" width="787" height="481" alt="">
<img class="u-sp" src="<?php echo get_template_directory_uri(); ?>/images/place/index/sp/img_cont_03.png" width="531" height="434" alt="">
<!-- .item-img // --></div>
<div class="item-txts">
<div class="item-txts-inner">
<p class="item-subttl">公園</p>
<ul class="item-list">
<li>花隈公園・・・・・・徒歩4分</li>
<li>下山手公園・・・・・徒歩3分</li>
<li>大倉山公園・・・・・・徒歩11分</li>
</ul>
<!-- .item-txts-inner // --></div>
<!-- .item-txts // --></div>
<!-- .item // --></div>

<div class="item box-imgright kindergarten" id="target-kindergerten">
<div class="item-img">
<img class="u-pc" src="<?php echo get_template_directory_uri(); ?>/images/place/index/img_cont_04.png" width="800" height="494" alt="">
<img class="u-sp" src="<?php echo get_template_directory_uri(); ?>/images/place/index/sp/img_cont_04.png" width="542" height="444" alt="">
<!-- .item-img // --></div>
<div class="item-txts">
<div class="item-txts-inner">
<p class="item-subttl">保育園・幼稚園・児童館</p>
<ul class="item-list">
<li>神戸元町ちどり保育園（連携保育園）・・徒歩8分</li>
<li class="item-url"><a href="http://www.chidori.or.jp/hoiku_kobemotomachi/" target="_blank">http://www.chidori.or.jp/hoiku_kobemotomachi/</a></li>
<li>神戸幼稚園・・・・・・徒歩10分</li>
<li>清風児童館・・・・・・徒歩8分</li>
<li>諏訪山児童館・・・・・徒歩10分</li>
</ul>
<!-- .item-txts-inner // --></div>
<!-- .item-txts // --></div>
<!-- .item // --></div>
<!-- .secbox // --></div>
<div class="footer-nav">
<ul>
<li><a href="/<?php echo get_home_url(); ?>/feature/" class="yellow-btn btn-shadow"><span><img src="<?php echo get_template_directory_uri(); ?>/shared/images/btn_feature.png" class="u-pc" alt="こぐまえんの特徴"><img src="<?php echo get_template_directory_uri(); ?>/shared/images/sp/btn_feature.png" class="u-sp" alt="こぐまえんの特徴"></span></a></li>
<li><a href="/<?php echo get_home_url(); ?>/life/" class="red-btn btn-shadow"><span><img src="<?php echo get_template_directory_uri(); ?>/shared/images/btn_life.png" class="u-pc" alt="こぐまえんでの生活"><img src="<?php echo get_template_directory_uri(); ?>/shared/images/sp/btn_life.png" class="u-sp" alt="こぐまえんでの生活"></span></a></li>
<li><a href="/<?php echo get_home_url(); ?>/about/" class="brown-btn btn-shadow"><span><img src="<?php echo get_template_directory_uri(); ?>/shared/images/btn_place.png" class="u-pc" alt="こぐまえん概要"><img src="<?php echo get_template_directory_uri(); ?>/shared/images/sp/btn_place.png" class="u-sp" alt="こぐまえん概要"></span></a></li>
</ul>
<!-- .footer-pagenav // --></div>


<!-- #Main // --></section>
<!-- #Content // --></section>
<!-- △メインコンテンツここまで // -->

<?php get_footer(); ?>

<!-- #Page // --></div>

<?php include( TEMPLATEPATH . '/gr_tag.php' ); ?>

</body>
</html>
