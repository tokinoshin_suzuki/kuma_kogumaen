<?php

// 使用しないメニューを非表示にする=================================================
function remove_admin_menus() {
	global $menu;
	// unsetで非表示にするメニューを指定
	//unset($menu[2]);		// ダッシュボード
	// unset($menu[5]);		// 投稿
	//unset($menu[10]);		// メディア
	//unset($menu[20]);		// 固定ページ
	unset($menu[25]);  // コメント
	//unset($menu[60]);		// 外観
	//unset($menu[65]);		// プラグイン
	//unset($menu[70]);		// ユーザー
	//unset($menu[75]);		// ツール
	//unset($menu[80]);		// 設定
}

add_action('admin_menu', 'remove_admin_menus');

//=============================================================================

//アイキャッチ
add_theme_support( 'post-thumbnails', array( 'post','blog' ) );

// 管理バーの非表示
add_filter('show_admin_bar', '__return_false');

// バージョン更新を非表示
add_filter('pre_site_transient_update_core', '__return_zero');

// APIによるバージョンチェックの通信をさせない
remove_action('wp_version_check', 'wp_version_check');

//プラグイン更新を非表示にする
remove_action('load-update-core.php', 'wp_update_plugins');
add_filter('pre_site_transient_update_plugins', create_function('$a', "return null;"));

//WordPressのバージョン情報
remove_action('wp_head', 'wp_generator');
//外部ツールを使ったブログ更新用のURL
remove_action('wp_head', 'rsd_link');
//wlwmanifestWindows Live Writerを使った記事投稿URL
remove_action('wp_head', 'wlwmanifest_link');
//デフォルトパーマリンクのURL
remove_action('wp_head', 'wp_shortlink_wp_head');
//前の記事と後の記事のURL
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head');
//RSSフィードのURL
remove_action('wp_head', 'feed_links_extra', 3);

add_filter( 'wpcf7_validate_configuration', '__return_false' );
//=============================================================================

// ベーシック認証
function basic_auth($auth_list,$realm="Restricted Area",$failed_text="認証に失敗しました。"){
if (isset($_SERVER['PHP_AUTH_USER']) and isset($auth_list[$_SERVER['PHP_AUTH_USER']])){
if ($auth_list[$_SERVER['PHP_AUTH_USER']] == $_SERVER['PHP_AUTH_PW']){
return $_SERVER['PHP_AUTH_USER'];
}
}
header('WWW-Authenticate: Basic realm="'.$realm.'"');
header('HTTP/1.0 401 Unauthorized');
header('Content-type: text/html; charset='.mb_internal_encoding());
die($failed_text);
}

// 名称変更
function change_post_menu_label() {
 global $menu;
 global $submenu;
 $menu[5][0] = '保護者向けブログ';
 $submenu['edit.php'][5][0] = '記事一覧';
 $submenu['edit.php'][10][0] = '新しい記事';
 $submenu['edit.php'][16][0] = 'タグ';
 //echo ";
}
function change_post_object_label() {
 global $wp_post_types;
 $labels = &$wp_post_types['post']->labels;
 $labels->name = '保護者向けブログ';
 $labels->singular_name = '保護者向けブログ';
 $labels->add_new = _x('追加', '保護者向けブログ');
 $labels->add_new_item = '記事の新規追加';
 $labels->edit_item = '記事の編集';
 $labels->new_item = '新規保護者向けブログ';
 $labels->view_item = '保護者向けブログを表示';
 $labels->search_items = '保護者向けブログを検索';
 $labels->not_found = '記事が見つかりませんでした';
 $labels->not_found_in_trash = 'ゴミ箱に記事は見つかりませんでした';
}
add_action( 'init', 'change_post_object_label' );
add_action( 'admin_menu', 'change_post_menu_label' );

?>