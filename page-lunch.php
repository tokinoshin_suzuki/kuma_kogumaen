<?php
/*
Template Name: こぐまえんの給食について
*/
?>
<?php get_header(); ?>

<div id="Page">

<?php include( TEMPLATEPATH . '/head.php' ); ?>
<!-- ▽メインコンテンツここから // -->
<section id="Content" class="g-content">
<div class="pagettl u-pc">
<h1><img src="<?php echo get_template_directory_uri(); ?>/images/lunch/common/page_ttl.png" width="360" height="30" alt="こぐまえんの給食について"></h1>
<!-- .pagettl // --></div>
<div class="pagettl u-sp">
<h1><img src="<?php echo get_template_directory_uri(); ?>/images/lunch/common/sp/page_ttl.jpg" width="640" height="180" alt="こぐまえんの給食について"></h1>
<!-- .pagettl // --></div>
<section id="Main" class="lunchBox">

	<nav id="pankuzu">
		<div class="waku">
			<div class="breadcrumbs" vocab="http://schema.org/" typeof="BreadcrumbList">
			<ul>
				<?php if(function_exists('bcn_display'))
				{
				bcn_display();
				}?>
			</ul>
			</div>
		</div>
	</nav>

	<div class="power">
		<h2 class="power-ttl"><img src="<?php echo get_template_directory_uri(); ?>/images/lunch/index/ttl_power_02.png" width="478" height="58" alt="こぐまえんの給食"></h2>
		<p class="power-txt mb">
			こぐまえんの給食は、隈病院の管理栄養士と調理師が作っています。<br />
			だから安心、安全。盛り付けも病院スタッフが園で行っています。
		</p>

		<h2 class="power-ttl"><img src="<?php echo get_template_directory_uri(); ?>/images/lunch/index/ttl_power.png" width="478" height="59" alt="こぐまえんの給食方針"></h2>
		<p class="power-txt">
			離乳食から幼児期へと移行するこの時期は、子どもたちにとって食事の基礎をつくる大切な時です。<br />
			そこでこぐまえんでは、保育士と管理栄養士が子どもたちの日々の成長を見て、食材や味付けを考えています。<br />
			食への興味を育むことが出来るように、一人ひとりに合わせた食事を提供することを心がけています。
		</p>
	<!-- .power // --></div>

	<div class="lunch-thinking">
		<div class="secttl ttl-thinking">
			<h2><img src="<?php echo get_template_directory_uri(); ?>/images/lunch/index/ttl_section01.png" Width="548" height="42" alt="給食に対する考え方"></h2>
		<!-- .secttl // --></div>

		<ul id="thinkingList" class="waku">
			<li>
				<div class="box">
					<h3 class="ttl-ss-thinking"><img src="<?php echo get_template_directory_uri(); ?>/images/lunch/index/ttl_lunch_thinking_01.png" Width="260" height="62" alt="栄養バランス"></h3>
					<div class="img-thinking">
						<figure><img src="<?php echo get_template_directory_uri(); ?>/images/lunch/index/img_lunch_thinking_01.png" Width="450" height="235" alt="栄養バランス"></figure>
					<!-- .img-thinking // --></div>
					<div class="txt-thinking">
						<p>
							給食は子どもたちの成長にとって大切な栄養源です。こぐまえんでは、「エネルギー源となる栄養素（糖質・脂質）」「身体をつくる栄養素（たんぱく質）」「身体の調子を整える栄養素（ビタミン・ミネラル）」をしっかり摂れるように、子どもたちの成長に合わせた栄養バランスを考えています。
						</p>
					<!-- .txt-thinking // --></div>
				<!-- .box // --></div>
			</li>
			<li>
				<div class="box">
					<h3 class="ttl-ss-thinking"><img src="<?php echo get_template_directory_uri(); ?>/images/lunch/index/ttl_lunch_thinking_02.png" Width="260" height="62" alt="薄味にする"></h3>
					<div class="img-thinking">
						<figure><img src="<?php echo get_template_directory_uri(); ?>/images/lunch/index/img_lunch_thinking_02.png" Width="450" height="235" alt="薄味にする"></figure>
					<!-- .img-thinking // --></div>
					<div class="txt-thinking">
						<p>
							子どもの味覚は大人の数倍敏感で、薄い味付けでもしっかり感じ取る事ができます。この時期に濃い味付けに慣れてしまうと味の変化を感じにくくなり、味覚の発達に影響することがあります。こぐまえんの給食は、毎朝カツオと昆布で出汁をとることから始まります。薄味にすることと、この出汁を使うことで素材の味を感じることができ、味覚の発達を助けます。
						</p>
					<!-- .txt-thinking // --></div>
				<!-- .box // --></div>
			</li>
			<li>
				<div class="box">
					<h3 class="ttl-ss-thinking"><img src="<?php echo get_template_directory_uri(); ?>/images/lunch/index/ttl_lunch_thinking_03.png" Width="330" height="62" alt="サイクルメニューで提供"></h3>
					<div class="img-thinking">
						<figure><img src="<?php echo get_template_directory_uri(); ?>/images/lunch/index/img_lunch_thinking_03.png" Width="450" height="255" alt="サイクルメニューで提供"></figure>
					<!-- .img-thinking // --></div>
					<div class="txt-thinking">
						<p>
							こぐまえんでは3週間のサイクルメニューを取り入れています。1週間や2週間のサイクルメニューの園が多い中、こぐまえんではよりたくさんのメニューに触れてもらうことができます。サイクルメニューにすることによって繰り返しのメニューに慣れ、食べる意欲を育み、料理や食材の名前を覚えることができ、食育にも繋がります。また、日々の子どもたちの食事の様子をみて改善策を立て、次回はより美味しく食べやすい給食を提供します。
						</p>
					<!-- .txt-thinking // --></div>
				<!-- .box // --></div>
			</li>
			<li>
				<div class="box">
					<h3 class="ttl-ss-thinking"><img src="<?php echo get_template_directory_uri(); ?>/images/lunch/index/ttl_lunch_thinking_04.png" Width="370" height="62" alt="病院も連携したチーム保育"></h3>
					<div class="img-thinking">
						<figure><img src="<?php echo get_template_directory_uri(); ?>/images/lunch/index/img_lunch_thinking_04.png" Width="450" height="255" alt="病院も連携したチーム保育"></figure>
					<!-- .img-thinking // --></div>
					<div class="txt-thinking">
						<p>
							保育士と管理栄養士で毎月給食会議を定例で開催しています。次月の献立や食材の共有、食育活動の検討などはもちろん、子どもたちの好きなメニュー、苦手なメニューの振り返りを行い、レシピの考案に生かしています。食べられなかったものを食べることができるようになったなど、管理栄養士も子どもたちの成長を、直に感じることができる貴重な機会です。また病院の管理層から安全性も含めてチェックを受ける体制も整えています。
						</p>
					<!-- .txt-thinking // --></div>
				<!-- .box // --></div>
			</li>
		<!-- #thinkingList // --></ul>
	<!-- .lunch-thinking // --></div>

	<div class="lunch-make">
		<div id="makeBox" class="waku">
			<div class="ttl-make">
				<h2><img src="<?php echo get_template_directory_uri(); ?>/images/lunch/index/ttl_make.png" Width="743" height="122" alt="隈病院の調理スタッフが担当しています。だから安心、安全。栄養価も考え美味しく作っています。"></h2>
			<!-- .ttl-make // --></div>

			<div class="txt-make top">
				<p>
					毎日病院の入院患者様へ安全・安心で美味しいお食事を提供している私たちは、いわば給食のプロです。<br />
					お子様のお食事については安心してお任せください。私たちの中には現在子育て真っ最中のお父さん、お母さんもいます。<br />
					園児たちの”食べる力”を育むために、メニューや調理について私たちの経験を活かした様々な工夫をしています。
				</p>
			<!-- .txt-make // --></div>

			<div class="ttl-make">
				<h2><img src="<?php echo get_template_directory_uri(); ?>/images/lunch/index/ttl_make_02.png" Width="779" height="61" alt="隈病院の給食はご好評いただいております。 "></h2>
			<!-- .ttl-make // --></div>

			<div class="txt-make">
				<p>
					隈病院では創業以来、病院の調理スタッフが患者様のお食事を手作りしています。3週間のサイクルメニューの中で、旬の野菜や魚を取り入れたり行事食を提供したりするなど、季節を感じられる献立を心がけています。
				</p>
			<!-- .txt-make // --></div>

			<div class="img-make">
				<figure><img src="<?php echo get_template_directory_uri(); ?>/images/lunch/index/img_make_01.png" Width="600" height="405" alt=""></figure>
			<!-- .img-make // --></div>

			<div class="txt-make btm">
				<p>
					私自身も、幼少期の頃のほとんどを保育園で過ごしました。私の保育園の記憶は「楽しかった」と「美味しかった」です。<br />
					行事食や季節の食べ物と色々ありましたが、特に手作りおやつがとても美味しかったのを今でも覚えています。そのおかげで今でも食べることが大好きです（笑）子どもたちにも、大人になっても思い出に残るような、美味しい食事を作っていきたいです。
				</p>
				<p class="tar">
					隈病院管理栄養士 林 昌樹
				</p>
			<!-- .txt-make // --></div>

			<div class="img-make-btm">
				<figure><img src="<?php echo get_template_directory_uri(); ?>/images/lunch/index/img_make_02.png" Width="594" height="671" alt=""></figure>
			<!-- .img-make // --></div>
		<!-- #makeBox // --></div>
	<!-- .lunch-make // --></div>

	<div id="nakayoshi">
		<div class="top-cont waku">
			<div class="left-cont">
				<div class="ttl-nakayoshi">
					<h2 class="u-pc"><img src="<?php echo get_template_directory_uri(); ?>/images/lunch/index/ttl_nakayoshi_01.png" Width="428" height="118" alt="アレルギー対策として「なかよし給食」を導入"></h2>
					<h2 class="u-sp"><img src="<?php echo get_template_directory_uri(); ?>/images/lunch/index/sp/ttl_nakayoshi_01.png" Width="580" height="139" alt="アレルギー対策として「なかよし給食」を導入"></h2>
				<!-- .ttl-make // --></div>
				<div class="txt-nakayoshi">
					<p>
						当園ではアレルギーのある子もない子も、みんなで同じ給食を食べられる「なかよし給食」を提供しております。
					</p>
				<!-- .txt-nakayoshi // --></div>
			<!-- .left-cont // --></div>
			<div class="right-cont">
				<figure><img src="<?php echo get_template_directory_uri(); ?>/images/lunch/index/img_nakayoshi_01.png" Width="490" height="374" alt=""></figure>
			<!-- .right-cont // --></div>
		<!-- .top-cont // --></div>

		<div class="about-cont waku">
			<div class="ttl-nakayoshi">
				<h2 class="u-pc"><img src="<?php echo get_template_directory_uri(); ?>/images/lunch/index/ttl_nakayoshi_02.png" Width="956" height="72" alt="こぐまえんの提供している「なかよし給食」とは"></h2>
				<h2 class="u-sp"><img src="<?php echo get_template_directory_uri(); ?>/images/lunch/index/sp/ttl_nakayoshi_02.png" Width="580" height="139" alt="こぐまえんの提供している「なかよし給食」とは"></h2>
			<!-- .ttl-make // --></div>
			<div class="txt-nakayoshi">
				<p>
					「卵アレルギー」「牛乳アレルギー」のお子さんが入園している期間は全園児に、それらの食品を除去した給食を提供します。<br />
					幼児アレルギーの原因物質の64%を占める「卵・牛乳」を除去することによって、アレルギー物質混入の事故のリスクを減らし、みんなで同じ給食を食べられるようにしております。<br />
					食材を除去するだけではなく、それらに変わる食材を使うことでより美味しく食べられるよう工夫し、たんぱく質やカルシウム等の栄養素が不足しない献立作成を心がけております。<br />
					調味料や加工品の成分についてはメーカーに問い合わせ、商品成分表にてアレルギー物質の確認をしています。<br />
					入園される前に管理栄養士との面談がございますので、「卵・牛乳」以外のアレルギーのお子さんについても、その時にご相談させていただきます。
				</p>
			<!-- .txt-nakayoshi // --></div>
		<!-- .about-cont // --></div>

		<div class="melit-cont waku">
			<div class="ttl-nakayoshi">
				<h2 class="u-pc"><img src="<?php echo get_template_directory_uri(); ?>/images/lunch/index/ttl_nakayoshi_03.png" Width="966" height="72" alt="なかよし給食におけるメリット"></h2>
				<h2 class="u-sp"><img src="<?php echo get_template_directory_uri(); ?>/images/lunch/index/sp/ttl_nakayoshi_03.png" Width="580" height="120" alt="なかよし給食におけるメリット"></h2>
			<!-- .ttl-make // --></div>

			<div class="melit-box">
				<div class="content">
					<div class="txt-nakayoshi">
						<p>
							アレルギー対応食が半減するので、アレルギーがある子もみんなと同じものが食べられるので疎外感を感じにくく、また食事の拾い食いによるリスクも軽減することができます。<br />
							アレルギーがない子は家庭と園で違う食材を使った料理を食べる事によって、より多くの食材に触れる機会が多くなります。
						</p>
					<!-- .txt-nakayoshi // --></div>
				<!-- .melit // --></div>

			<!-- .melit-box // --></div>
		<!-- .melit-cont // --></div>
	<!-- #nakayoshi // --></div>

	<div class="lunch-gallery">
		<div class="secttl ttl-gallery">
			<h2><img src="<?php echo get_template_directory_uri(); ?>/images/lunch/index/ttl_section02.png" Width="476" height="42" alt="献立ギャラリー"></h2>
		</div>

		<ul id="galleryList" class="waku fixBox">
			<?php
				$fields = $cfs->get('list-food');
				foreach ($fields as $field) :
			?>
			<li class="fixItem">
				<div class="box">
					<?php
					$check = $field['img-food'];
					if($check) :?>
					<figure><img src="<?php echo $field['img-food']; ?>" Width="200" height="200" alt="<?php echo $field['ttl-food']; ?>"></figure>
					<?php else : ?>
					<img src="<?php echo get_template_directory_uri(); ?>/images/noimages.png" Width="200" height="200" alt="NO IMAGE">
					<?php endif; ?>
					<div class="ttl-g"><?php echo $field['ttl-food']; ?></div>
				</div>
			</li>
			<?php endforeach; ?>
		</ul>
	</div>

	<div class="lunch-recipe">
		<div class="secttl ttl-recipe">
			<h2><img src="<?php echo get_template_directory_uri(); ?>/images/lunch/index/ttl_section03.png" Width="476" height="42" alt="行事食ギャラリー"></h2>
		</div>

		<p class="recipe_txt tac waku">行事を大切にし、関心が持てるよう、季節も感じられる行事食にも力を入れています。</p>

		<ul id="galleryList" class="waku fixBox">
			<?php
				$fields = $cfs->get('list-event');
				foreach ($fields as $field) :
			?>
			<li class="fixItem">
				<div class="box">
					<?php
					$check = $field['img-event'];
					if($check) :?>
					<figure><img src="<?php echo $field['img-event']; ?>" Width="200" height="200" alt="<?php echo $field['ttl-event']; ?>"></figure>
					<?php else : ?>
					<img src="<?php echo get_template_directory_uri(); ?>/images/noimages.png" Width="200" height="200" alt="NO IMAGE">
					<?php endif; ?>
					<div class="ttl-g"><?php echo $field['ttl-event']; ?></div>
				</div>
			</li>
			<?php endforeach; ?>
		</ul>
	</div>

	<div class="lunch-oyatsu">
		<div class="oyatsu-box waku">
			<div class="left-oyatsu">
				<h2 class="ttl-oyatsu u-pc"><img src="<?php echo get_template_directory_uri(); ?>/images/lunch/index/ttl_oyatsu.png" Width="264" height="28" alt="こぐまえんのおやつ"></h2>
				<h2 class="ttl-oyatsu u-sp"><img src="<?php echo get_template_directory_uri(); ?>/images/lunch/index/sp/ttl_oyatsu.png" Width="580" height="49" alt="こぐまえんのおやつ"></h2>
				<div class="txt-oyatsu">
					<p>
						3時のおやつは園児にとって、楽しく過ごした1日を締めくくる重大なイベントです。こぐまえんでは手作りおやつや、青果店から仕入れる果物を提供しています。手作りおやつでは、牛乳や野菜、果物の甘さを活かしたメニューを取り入れ、優しい味に仕上げています。<br />
						市販のお菓子も美味しいですが、手作りのおやつでしかだせない美味しさを子どもたちに感じてもらいたいと思っております。
					</p>
				</div>
			<!-- .left-oyatsu // --></div>
			<div class="right-oyatsu">
				<figure><img src="<?php echo get_template_directory_uri(); ?>/images/lunch/index/img_oyatsu.png" Width="511" height="452" alt=""></figure>
			<!-- .right-oyatsu // --></div>
		<!-- .oyatsu-box // --></div>
	<!-- .lunch-oyatsu // --></div>

	<div class="oyatsu-gallery">
		<div class="ttl-oyatsu-gallery">
			<h2 class="u-pc"><img src="<?php echo get_template_directory_uri(); ?>/images/lunch/index/ttl_oyatsu_gallery.png" Width="246" height="49" alt="おやつギャラリー"></h2>
			<h2 class="u-sp"><img src="<?php echo get_template_directory_uri(); ?>/images/lunch/index/sp/ttl_oyatsu_gallery.png" Width="580" height="87" alt="おやつギャラリー"></h2>
		</div>

		<ul id="galleryList" class="waku fixBox">
			<?php
				$fields = $cfs->get('list-oyathu');
				foreach ($fields as $field) :
			?>
			<li class="fixItem">
				<div class="box">
					<?php
					$check = $field['img-oyathu'];
					if($check) :?>
					<figure><img src="<?php echo $field['img-oyathu']; ?>" Width="200" height="200" alt="<?php echo $field['ttl-oyathu']; ?>"></figure>
					<?php else : ?>
					<img src="<?php echo get_template_directory_uri(); ?>/images/noimages.png" Width="200" height="200" alt="NO IMAGE">
					<?php endif; ?>
					<div class="ttl-g"><?php echo $field['ttl-oyathu']; ?></div>
				</div>
			</li>
			<?php endforeach; ?>
		</ul>
	</div>

	<div class="footer-nav">
	<ul>
	    <li><a href="<?php echo get_home_url(); ?>/life/" class="red-btn btn-shadow"><span><img src="<?php echo get_template_directory_uri(); ?>//shared/images/btn_life.png" class="u-pc" alt="こぐまえんでの生活"><img src="<?php echo get_template_directory_uri(); ?>//shared/images/sp/btn_life.png" class="u-sp" alt="こぐまえんでの生活"></span></a></li>
	  <li><a href="<?php echo get_home_url(); ?>/place/" class="green-btn btn-shadow"><span><img src="<?php echo get_template_directory_uri(); ?>//shared/images/btn_flow.png" class="u-pc" alt="周辺環境"><img src="<?php echo get_template_directory_uri(); ?>//shared/images/sp/btn_flow.png" class="u-sp" alt="周辺環境"></span></a></li>
	    <li><a href="<?php echo get_home_url(); ?>/about/" class="brown-btn btn-shadow"><span><img src="<?php echo get_template_directory_uri(); ?>//shared/images/btn_place.png" class="u-pc" alt="こぐまえん概要"><img src="<?php echo get_template_directory_uri(); ?>//shared/images/sp/btn_place.png" class="u-sp" alt="こぐまえん概要"></span></a></li>
	</ul>
	<!-- .footer-pagenav // --></div>

<!-- #Main // --></section>
<!-- #Content // --></section>
<!-- △メインコンテンツここまで // -->

<?php get_footer(); ?>

<!-- #Page // --></div>

<?php include( TEMPLATEPATH . '/gr_tag.php' ); ?>

</body>
</html>