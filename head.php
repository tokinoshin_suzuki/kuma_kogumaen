<script>
$(window).on("scroll", function(){
    $("#GlobalHeader").css("left", -$(window).scrollLeft());
});
</script>
<header id="GlobalHeader">
<div class="wrap-gnavi" id="#GlobalNavi">
<div class="inner u-pc">
<ul class="gnavi all-rover">
<li id="gnavi01"><a class="rover" href="<?php echo get_home_url(); ?>/concept/"><img src="<?php echo get_template_directory_uri(); ?>/shared/images/header_navi01.png" alt="保育理念"></a></li>
<li class="js-megaMenu-trigger01" id="gnavi02"><a class="rover" href="<?php echo get_home_url(); ?>/feature/"><img src="<?php echo get_template_directory_uri(); ?>/shared/images/header_navi02.png" alt="こぐまえんの特徴"></a></li>
<li class="js-megaMenu-trigger02" id="gnavi03"><a class="rover" href="<?php echo get_home_url(); ?>/life/"><img src="<?php echo get_template_directory_uri(); ?>/shared/images/header_navi03.png" alt="こぐまえんでの生活"></a></li>
<li><h1><a href="<?php echo get_home_url(); ?>/"><img src="<?php echo get_template_directory_uri(); ?>/shared/images/logo_header.png" alt="こぐまえん"></a></h1></li>
<li id="gnavi04"><a class="rover" href="<?php echo get_home_url(); ?>/place/"><img src="<?php echo get_template_directory_uri(); ?>/shared/images/header_navi04.png" alt="周辺環境"></a></li>
<li id="gnavi05"><a class="rover" href="<?php echo get_home_url(); ?>/flow/"><img src="<?php echo get_template_directory_uri(); ?>/shared/images/header_navi05.png" alt="入園案内"></a></li>
<li id="gnavi06"><a class="rover" href="<?php echo get_home_url(); ?>/about/"><img src="<?php echo get_template_directory_uri(); ?>/shared/images/header_navi06.png" alt="こぐまえん概要"></a></li>
</ul>
<!-- .inner // --></div>
<div class="inner u-sp">
<h1 class="header-logo"><a href="<?php echo get_home_url(); ?>/"><img src="<?php echo get_template_directory_uri(); ?>/shared/images/sp/logo_header.png" alt="こぐまえん"></a></h1>
<div class="sp-toggle"><a href="javascript:void(0)">
<i></i>
<p class="navi-open">MENU</p><p class="navi-close"></a>
<!-- .sp-toggle // --></div>
<ul class="gnavi-sp">
<li><a href="<?php echo get_home_url(); ?>/concept/">保育理念</a></li>
<li class="toggle-caregory">
<p><span>こぐまえんの特徴</span></p>
</li>
<li class="category-small">
<ul>
<li><a href="<?php echo get_home_url(); ?>/feature/?id=target-about">内閣府認可保育園について</a></li>
<li><a href="<?php echo get_home_url(); ?>/feature/?id=target-building">園舎のこだわり</a></li>
<li><a href="<?php echo get_home_url(); ?>/feature/?id=target-environment">整った知育環境</a></li>
<li><a href="<?php echo get_home_url(); ?>/feature/?id=target-security">徹底した安全対策</a></li>
</ul>
</li>
<li class="toggle-caregory"><p><span>こぐまえんでの生活</span></p></li>
<li class="category-small">
<ul>
<li><a href="<?php echo get_home_url(); ?>/life/?id=target-flow">こぐまえんでの1日の流れ
</a></li>
<li><a href="<?php echo get_home_url(); ?>/life/?id=target-oneyear">こぐまえんでの1年</a></li>
<li><a href="<?php echo get_home_url(); ?>/lunch/">こぐまえんの給食について</a></li>
<li><a href="<?php echo get_home_url(); ?>/blog/">こぐまえんブログ</a></li>
</ul>
</li>
<li><a href="<?php echo get_home_url(); ?>/place/">周辺環境</a></li>
<li><a href="<?php echo get_home_url(); ?>/flow/">入園案内</a></li>
<li><a href="<?php echo get_home_url(); ?>/about/">こぐまえん概要</a></li>
<li><a href="<?php echo get_home_url(); ?>/privacy/">プライバシーポリシー</a></li>
</ul>
<div class="overlay"></div>
<!-- .inner // --></div>
<!-- .wrap-gnavi // --></div>

<div class="wrap-gnavi-smallClass">
<div class="gnavi-smallClass js-megaMenu" id="megamenu01">
<ul>
<li><a href="<?php echo get_home_url(); ?>/feature/?id=target-about"><img src="<?php echo get_template_directory_uri(); ?>/shared/images/header_navi_feature01.png"><span>内閣府認可保育園について</span></a></li>
<li><a href="<?php echo get_home_url(); ?>/feature/?id=target-building"><img src="<?php echo get_template_directory_uri(); ?>/shared/images/header_navi_feature02.png"><span>園舎のこだわり</span></a></li>
<li><a href="<?php echo get_home_url(); ?>/feature/?id=target-environment"><img src="<?php echo get_template_directory_uri(); ?>/shared/images/header_navi_feature03.png"><span>整った知育環境</span></a></li>
<li><a href="<?php echo get_home_url(); ?>/feature/?id=target-security"><img src="<?php echo get_template_directory_uri(); ?>/shared/images/header_navi_feature04.png"><span>徹底した安全対策</span></a></li>
</ul>
<!-- .gnavi_smallClass // --></div>
<div class="gnavi-smallClass js-megaMenu" id="megamenu02">
<ul>
<li><a href="<?php echo get_home_url(); ?>/life/?id=target-flow"><img src="<?php echo get_template_directory_uri(); ?>/shared/images/header_navi_life01.png"><span>こぐまえんでの1日の流れ</span></a></li>
<li><a href="<?php echo get_home_url(); ?>/life/?id=target-oneyear"><img src="<?php echo get_template_directory_uri(); ?>/shared/images/header_navi_life02.png"><span>こぐまえんでの1年</span></a></li>
<li><a href="<?php echo get_home_url(); ?>/lunch/"><img src="<?php echo get_template_directory_uri(); ?>/shared/images/header_navi_life03.png"><span>こぐまえんの給食について</span></a></li>
<li><a href="<?php echo get_home_url(); ?>/blog/"><img src="<?php echo get_template_directory_uri(); ?>/shared/images/header_navi_life04.png"><span>こぐまえんブログ</span></a></li>
</ul>
<!-- .gnavi_smallClass // --></div>
<!-- .wrap-gnavi-smallClass // --></div>
<!-- #GlobalHeader // --></header>