<?php
if(get_post_type() == 'post' ):
  $userArray = array(
    "kogumaenweb" => "HpykGmstR2118"
  );
  basic_auth($userArray);
endif;

if(!is_home()):
  if(is_category('news') || in_category('news')) :
  $userArray = array(
    "kogumaenweb" => "HpykGmstR2118"
  );
  basic_auth($userArray);
  endif;
endif;
?>
<?php include( TEMPLATEPATH . '/config.php' ); ?>
<!DOCTYPE html>
<html lang="ja">
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#">
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<?php if ( is_home() || is_front_page() ) : ?>
<script src="<?php echo get_template_directory_uri(); ?>/shared/js/smoothScrollEx.js"></script>
<?php endif; ?>
<title><?php echo $title; ?></title>
<meta name="description" content="<?php echo $description; ?>">
<meta name="keywords" content="<?php echo $keywords; ?>">
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_uri(); ?>" />

<meta property="og:type" content="website">
<meta property="og:url" content="URL">
<meta property="og:site_name" content="こぐまえん | 神戸市中央区の内閣府認定の保育園">
<meta property="og:title" content="<?php echo $title; ?>">
<meta property="og:description" content="<?php echo $keywords; ?>">
<meta property="og:image" content="">
<meta property="og:locale" content="ja_JP">
<meta property="fb:app_id" content="">
<meta name="robots" content="index,follow">
<?php include( TEMPLATEPATH . '/head-meta.php' ); ?>
<?php if ( is_home() || is_front_page() ) : ?>
<script src="<?php echo get_template_directory_uri(); ?>/js/index.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/sliderPro/jquery.sliderPro.js"></script>
<?php endif; ?>
<?php include( TEMPLATEPATH . '/tag-m1.php' ); ?>
<!-- ▽head -->
<?php wp_head(); ?>
<!-- ▲head -->
<meta name="google-site-verification" content="hUPi5dSU_uH6XfWesBZKyD4K7EuVq5E4Oi_OQW0utBE" />
</head>

<body>
<?php include( TEMPLATEPATH . '/fb_root.php' ); ?>
<?php include( TEMPLATEPATH . '/tag-m2.php' ); ?>
<!-- headerここまで -->

<div id="Page">
<?php include( TEMPLATEPATH . '/head.php' ); ?>
