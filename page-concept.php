<?php get_header(); ?>

<div id="Page">
<?php include( TEMPLATEPATH . '/head.php' ); ?>

<!-- ▽メインコンテンツここから // -->
<section id="Content" class="g-content">
<div class="pagettl u-pc">
<h1><img src="<?php echo get_template_directory_uri(); ?>/images/concept/common/page_ttl.png" width="301" height="30" alt="こぐまえんの保育理念"></h1>
<!-- .pagettl // --></div>
<div class="pagettl u-sp">
<h1><img src="<?php echo get_template_directory_uri(); ?>/images/concept/common/sp/page_ttl.jpg" width="640" height="180" alt="こぐまえんの保育理念"></h1>
<!-- .pagettl // --></div>

<section id="Main">

<div class="power">
<h2 class="power-ttl"><img src="<?php echo get_template_directory_uri(); ?>/images/concept/index/ttl_power.png" width="384" height="69" alt="生きるちから"></h2>
<p class="power-txt">子どもたちがこれからの未来を切り開いていくために最も大切なことは<br class="u-pc">
「生きる力」培うことだとこぐまえんは考えています。<br>
この「生きる力」を育むため「あたま・こころ・からだ」全身を使い、<br class="u-pc">
いきいきとそして健やかに毎日を過ごし、自信と意欲、<br class="u-pc">
興味をもって未来を生きる力を培っていく。子ども一人ひとりに寄り添い、<br class="u-pc">
ご家族と共により良い保育が実施できるようにと考えております。</p>
<!-- .power // --></div>

<div class="head">
<div class="head-box">
<h2 class="head-ttl u-pc"><img src="<?php echo get_template_directory_uri(); ?>/images/concept/index/ttl_head.png" width="241" height="36" alt="あたま"></h2>
<h2 class="head-ttl u-sp"><img src="<?php echo get_template_directory_uri(); ?>/images/concept/index/sp/ttl_head.jpg" alt="あたま"></h2>
<p class="head-txt">園生活で規則的な生活を送ることにより基礎的な生活習慣が<br class="u-pc">
養われ、態度を培い自身のことができるよう力を身につけます。<br>
遊びに工夫をこらし子どもが作ることができる環境を整え自分で考えることにより知的興味を満足させます。<br>
自然に対する興味や関心を活動を通して引き出し、考える力、わかる力、観る力を培い子どもの探究心や好奇心を旺盛にします。<br>
絵本や紙芝居などの読み聞かせで想像力や集中力を伸ばし、豊かな体験をすることにより知る喜びや学ぶ面白さを身につけます。</p>
<!-- .head-box // --></div>
<!-- .head // --></div>

<div class="body">
<div class="body-box">
<h2 class="body-ttl u-pc"><img src="<?php echo get_template_directory_uri(); ?>/images/concept/index/ttl_body.png" width="241" height="38" alt="からだ"></h2>
<h2 class="body-ttl u-sp"><img src="<?php echo get_template_directory_uri(); ?>/images/concept/index/sp/ttl_body.jpg" alt="からだ"></h2>
<p class="body-txt">大きな吹き抜けのあるこぐまえんはとても明るく清潔で<br class="u-pc">
家庭的な雰囲気なので、子どもも安心して過ごせます。<br>
歩く、走る、跳ぶなどの基本的な運動機能の発達を<br class="u-pc">
促す園生活を送ることにより楽しみながら心を落ち着かせ<br class="u-pc">
意欲的に遊ぶことができます。<br>
身近な自然にふれたり、リズム体操などで元気で明るくたくましい<br class="u-pc">
体作りをするとともに感性豊かな子どもに育てます。<br>
また、親御様とともに連携しながら一人ひとりの健康に十分に気をつけ、<br class="u-pc">子どもが自分から健康で過ごせ増進を図る知恵を身につけます。</p>
<!-- .body-box // --></div>
<!-- .body // --></div>

<div class="heart">
<div class="heart-box">
<h2 class="heart-ttl u-pc"><img src="<?php echo get_template_directory_uri(); ?>/images/concept/index/ttl_heart.png" width="242" height="33" alt="こころ"></h2>
<h2 class="heart-ttl u-sp"><img src="<?php echo get_template_directory_uri(); ?>/images/concept/index/sp/ttl_heart.jpg" alt="こころ"></h2>
<p class="heart-txt">子どもにとっては初めての集団生活の場です。<br>
当然、子ども同士のぶつかり合いやがまんも<br class="u-pc">
子どもにとっては大切な経験となります。<br>
そんな経験をしながら自主性や協調性など社会的な振る舞い方や<br class="u-pc">
まわりに迷惑をかけないなどといった道徳性の<br class="u-pc">
芽生えを大切に育てていきます。<br>
集団での遊びや地域社会の人とかかわる体験をする中で人に対する、相手を思いやる気持ちや信じる気持ちを培います。<br>
そして命、人権を尊重する思いやりの心を養います。</p>
<!-- .heart-box // --></div>
<!-- .heart // --></div>

<div class="hospital">
<div class="hospital-img">
<img src="<?php echo get_template_directory_uri(); ?>/images/concept/index/img_hospital.jpg" class="u-sp" alt="隈病院">
<!-- .hospital-img // --></div>
<div class="hospital-txt">
<p class="hospital-catch">子どもの<span>健康・安全</span>を第一に考え、<br><span>衛生的な環境</span>で<br class="u-sp">子どもの成長をサポートします。</p>
<p>医療に準ずる隈病院が設立した園だからこそ、特に衛生面には配慮をしています。<br>
お子様の安全のため、災害にも備え建物の強度も考慮して園舎を作りました。<br>
万一のときのため保育スタッフへの防災訓練なども実施。<br>
隈病院も一丸となってお子様の成長をサポートする環境を整えています。</p>
<!-- .hospital-txt // --></div>
<!-- .hospital // --></div>

<div class="footer-nav">
<ul>
	<li><a href="<?php echo get_home_url(); ?>/feature/#target-about" class="orange-btn btn-shadow"><span><img src="<?php echo get_template_directory_uri(); ?>/images/concept/index/btn_company.png" class="u-pc" alt="内閣府認可保育園について"><img src="<?php echo get_template_directory_uri(); ?>/images/concept/index/sp/btn_company.png" class="u-sp" alt="内閣府認可保育園について"></span></a></li>
    <li><a href="<?php echo get_home_url(); ?>/feature/#target-building" class="blue-btn btn-shadow"><span><img src="<?php echo get_template_directory_uri(); ?>/images/concept/index/btn_building.png" class="u-pc" alt="園舎のこだわり"><img src="<?php echo get_template_directory_uri(); ?>/images/concept/index/sp/btn_building.png" class="u-sp" alt="園舎のこだわり"></span></a></li>
    <li><a href="<?php echo get_home_url(); ?>/feature/#target-environment" class="red-btn btn-shadow"><span><img src="<?php echo get_template_directory_uri(); ?>/images/concept/index/btn_environment.png" class="u-pc" alt="整った知育環境"><img src="<?php echo get_template_directory_uri(); ?>/images/concept/index/sp/btn_environment.png" class="u-sp" alt="整った知育環境"></span></a></li>
    <li><a href="<?php echo get_home_url(); ?>/feature/#target-security" class="green-btn btn-shadow"><span><img src="<?php echo get_template_directory_uri(); ?>/images/concept/index/btn_safety.png" class="u-pc" alt="徹底した安全対策"><img src="<?php echo get_template_directory_uri(); ?>/images/concept/index/sp/btn_safety.png" class="u-sp" alt="徹底した安全対策"></span></a></li>
</ul>
<!-- .footer-pagenav // --></div>

<!-- #Main // --></section>
<!-- #Content // --></section>
<!-- △メインコンテンツここまで // -->

<?php get_footer(); ?>

<!-- #Page // --></div>

<?php include( TEMPLATEPATH . '/gr_tag.php' ); ?>


</body>
</html>
