<?php get_header(); ?>

<div id="Page">

<?php include( TEMPLATEPATH . '/head.php' ); ?>
<!-- ▽メインコンテンツここから // -->
<section id="Content" class="g-content">
<div class="pagettl u-pc">
<h1><img src="<?php echo get_template_directory_uri(); ?>/images/about/common/page_ttl.png" width="206" height="30" alt="こぐまえん概要"></h1>
<!-- .pagettl // --></div>
<div class="pagettl u-sp">
<h1><img src="<?php echo get_template_directory_uri(); ?>/images/about/common/sp/page_ttl.jpg" width="640" height="180" alt="こぐまえん概要"></h1>
<!-- .pagettl // --></div>
<section id="Main">
<div class="secttl ttl-idea">
<h2><img src="<?php echo get_template_directory_uri(); ?>/images/about/index/ttl_section01.png" Width="618" height="42" alt="こぐまえん設立への想い"></h2>
<p class="secttl-txt">子どもたちの心に寄り添い、ご家族の皆様と共に</p>
<!-- .secttl // --></div>
<div class="secbox idea">
<div class="itembox ideabox">
<div class="item-txt">
<div class="img-left u-pc">
<img src="<?php echo get_template_directory_uri(); ?>/images/about/index/img_idea01.png" width="281" height="320" alt="">
<!-- .item-img // --></div>
<p>保育園は子どもたちの生涯にわたる人間形成にとって極めて重要な時期に、日中の大半を過ごすとても大事な居場所です。</p>
<p>こぐまえんでは子供たちにとって、心も体も十分に安心して落ち着ける場所、すなわち第二の我が家として、衛生的で安全な環境の下、ひとりひとりが安心感と信頼感を持って活動できるよう、そして、個々の発達に即した環境の中で、健全な心身の育成と思いやりの心が育つよう、私たち保育士が暖かい眼差しと温もりをもって子どもたちの心に寄り添い、丁寧で手厚い保育を実践してゆき、ご家族の皆様と共に健やかな成長を一緒に見守ってまいります。</p>
<div class="img-right u-pc">
<img src="<?php echo get_template_directory_uri(); ?>/images/about/index/img_idea02.png" width="281" height="326" alt="">
<!-- .item-img // --></div>
<!-- .item-txt // --></div>
<div class="item-img u-sp">
<img src="<?php echo get_template_directory_uri(); ?>/images/about/index/sp/img_idea.png" width="468" height="314" alt="">
<!-- .item-img // --></div>
<!-- .itembox .ideabox // --></div>
<!-- .secbox .idea // --></div>

<div class="secttl ttl-about">
<h2><img src="<?php echo get_template_directory_uri(); ?>/images/about/index/ttl_section02.png" Width="468" height="42" alt="こぐまえん概要"></h2>
<!-- .secttl // --></div>
<div class="secbox about">
<div class="itembox aboutbox">
<div class="item-img">
<img class="img-about" src="<?php echo get_template_directory_uri(); ?>/images/about/index/img_about01.png" Width="470" height="340" alt="">
<!-- .item-img // --></div>
<div class="item-img">
<img class="img-about" src="<?php echo get_template_directory_uri(); ?>/images/about/index/img_about02.png" Width="470" height="340" alt="">
<!-- .item-img // --></div>
<!-- .itembox .aboutbox // --></div>
<table class="item-table u-pc" width="980" height="360">
<tbody>
<tr>
<th>所在地</th><td>〒650-0013<br>神戸市中央区花隈町 21-18</td>
<th>対象年齢</th><td>生後6ヶ月から3歳未満の乳幼児<br>（在籍する年度の4月1日の年齢）</td>
</tr>
<tr>
<th>施設</th><td>延床面積 98.81㎡（建築確認面積）</td>
<th>開園日</th><td>月曜日〜土曜日</td>
</tr>
<tr>
<th>構造</th><td>木造2階建</td><th>休園日</th><td>日曜・祝日・年末年始</td>
</tr>
<tr>
<th>設置者</th><td>医療法人神甲会 隈病院</td>
<th rowspan="2">保育時間</th><td rowspan="2" class="data-time">基本保育 7:00〜18:00<br>
延長保育 18:00〜19:00</td>
</tr>
<tr><th>定員</th><td>10名程度</td></tr>
</tbody>
</table>
<table class="item-table u-sp" width="490" height="360">
<tbody>
<tr>
<th>所在地</th><td>〒650-0013<br>神戸市中央区花隈町 21-18</td>
</tr>
<tr>
<th>施設</th><td>延床面積 98.81㎡（建築確認面積）</td>
</tr>
<tr>
<th>構造</th><td>木造2階建</td>
</tr>
<tr>
<th>設置者</th><td>医療法人神甲会 隈病院</td>
</tr>
<tr><th>定員</th><td>10名程度</td></tr>
<tr>
<th>対象年齢</th><td>生後6ヶ月から3歳未満の乳幼児<br>（在籍する年度の4月1日の年齢）</td>
</tr>
<tr>
<th>開園日</th><td>月曜日〜土曜日</td>
</tr>
<tr>
<th>休園日</th><td>日曜・祝日・年末年始</td>
</tr>
<tr>
<th rowspan="2">保育時間</th><td rowspan="2" class="data-time">基本保育 7:00〜18:00<br>
延長保育 18:00〜19:00</td>
</tr>
</tbody>
</table>
<!-- .secbox .about // --></div>
<div class="item-map">
 <div id="map"></div>
 <!-- .item-map // --></div>

<div class="footer-nav">
<ul>
<li><a href="<?php echo get_home_url(); ?>/concept/" class="blue-btn btn-shadow"><span><img src="<?php echo get_template_directory_uri(); ?>/shared/images/btn_concept.png" class="u-pc" alt="保育理念"><img src="<?php echo get_template_directory_uri(); ?>/shared/images/sp/btn_concept.png" class="u-sp" alt="保育理念"></span></a></li>
<li><a href="<?php echo get_home_url(); ?>/life/" class="red-btn btn-shadow"><span><img src="<?php echo get_template_directory_uri(); ?>/shared/images/btn_life.png" class="u-pc" alt="こぐまえんでの生活"><img src="<?php echo get_template_directory_uri(); ?>/shared/images/sp/btn_life.png" class="u-sp" alt="こぐまえんでの生活"></span></a></li>
<li><a href="<?php echo get_home_url(); ?>/place/" class="green-btn btn-shadow"><span><img src="<?php echo get_template_directory_uri(); ?>/shared/images/btn_flow.png" class="u-pc" alt="周辺環境"><img src="<?php echo get_template_directory_uri(); ?>/shared/images/sp/btn_flow.png" class="u-sp" alt="周辺環境"></span></a></li>
</ul>
<!-- .footer-pagenav // --></div>


<!-- #Main // --></section>
<!-- #Content // --></section>
<!-- △メインコンテンツここまで // -->

<?php get_footer(); ?>

<!-- #Page // --></div>

<?php include( TEMPLATEPATH . '/gr_tag.php' ); ?>

</body>
</html>