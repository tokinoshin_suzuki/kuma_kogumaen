<?php get_header(); ?>

<div id="Page">

<?php include( TEMPLATEPATH . '/head.php' ); ?>
<!-- ▽メインコンテンツここから // -->
<section id="Content" class="g-content">
<div class="pagettl u-pc">
<h1><img src="<?php echo get_template_directory_uri(); ?>/images/blog/common/page_ttl.png" width="236" height="28" alt="こぐまえんだより"></h1>
<!-- .pagettl // --></div>
<div class="pagettl u-sp">
<h1><img src="<?php echo get_template_directory_uri(); ?>/images/blog/common/sp/page_ttl.jpg" width="640" height="180" alt="こぐまえんだより"></h1>
<!-- .pagettl // --></div>
<section id="Main" class="category">

<nav id="pankuzu">
	<div class="waku">
		<div class="breadcrumbs" vocab="http://schema.org/" typeof="BreadcrumbList">
		<ul>
			<?php if(function_exists('bcn_display'))
			{
			bcn_display();
			}?>
		</ul>
		</div>
	</div>
</nav>

<div id="entryWrap" class="waku">
	<div id="entryMain">
		<div class="news-list">
			<ul id="entryList" class="fixBox">
				<?php if(have_posts()): while(have_posts()): the_post(); ?>
				<li class="fixItem">
					<a href="<?php the_permalink(); ?>">
						<div class="img-entry">
							<?php if(has_post_thumbnail()): ?>
							<figure><?php the_post_thumbnail('thumbnail'); ?></figure>
							<?php else: ?>
							<img src="<?php echo get_template_directory_uri(); ?>/images/noimages.png" Width="200" height="200" alt="NO IMAGE">
							<?php endif; ?>
						</div>
						<?php
							if ($terms = get_the_terms($post->ID, 'blogcat')) {
								foreach ( $terms as $term ) {
									echo '<span class="cat">' . esc_html($term->name) . '</span>';
								}
							}
						?>
						<div><span class="time"><time datetime="<?php the_time( 'Y.m.d' ); ?>" pubdate="pubdate"><?php the_time( 'Y/m/d' ); ?></time></span></div>
						<h2 class="ttl-entry"><?php the_title(); ?></h2>
					</a>
				</li>
				<?php endwhile; ?>
				<?php else : ?>
				<?php endif; ?>
				<?php wp_reset_postdata(); ?>
			<!-- #entryList // --></ul>

			<div id="pagenav">
				<?php if(function_exists(wp_pagenavi)) { wp_pagenavi(); } ?>
			<!-- #pagenav // --></div>

		<!-- .news-list // --></div>
	<!-- #entryMain // --></div>

	<div id="entrySide">
		<div class="side-box side-category">
			<h3 class="side-ttl u-pc"><img src="<?php echo get_template_directory_uri(); ?>/images/news/common/ttl_category.png" width="230" height="55" alt="カテゴリー"></h3>
			<h3 class="side-ttl u-sp"><img src="<?php echo get_template_directory_uri(); ?>/images/news/common/sp/ttl_category.png" width="640" height="76" alt="カテゴリー"></h3>
			<ul class="side-list-cont">
				<?php
					$terms = get_terms('blogcat');
					foreach ( $terms as $term ) {
						echo '<li>';
						echo '<a href="'.get_term_link($term).'">'.$term->name.'</a>';
						echo '</li>';
					}
				?>
			</ul>
		<!-- .side-box // --></div>

		<div class="side-box side-archives">
			<h3 class="side-ttl u-pc"><img src="<?php echo get_template_directory_uri(); ?>/images/news/common/ttl_archives.png" width="230" height="55" alt="アーカイブ"></h3>
			<h3 class="side-ttl u-sp"><img src="<?php echo get_template_directory_uri(); ?>/images/news/common/sp/ttl_archives.png" width="640" height="76" alt="アーカイブ"></h3>
			<ul class="side-list-cont">
				<?php wp_get_archives('post_type=blog&type=monthly&show_post_count=true'); ?>
			</ul>
		<!-- .side-box // --></div>

		<div class="side-box side-newentry">
			<h3 class="side-ttl u-pc"><img src="<?php echo get_template_directory_uri(); ?>/images/news/common/ttl_newentry.png" width="230" height="55" alt="最近の記事"></h3>
			<h3 class="side-ttl u-sp"><img src="<?php echo get_template_directory_uri(); ?>/images/news/common/sp/ttl_newentry.png" width="640" height="76" alt="最近の記事"></h3>
			<ul class="side-list-cont">
				<?php
				$posts = get_posts(array(
				'post_type' => 'blog',
				'posts_per_page' => 5,
				'order'=>'DESC'
				));
				?>
				<?php if($posts): foreach($posts as $post): setup_postdata($post); ?>
				<li>
					<span class="time"><time datetime="<?php the_time( 'Y.m.d' ); ?>" pubdate="pubdate"><?php the_time( 'Y/m/d' ); ?></time></span>
					<h4 class="ttl-entry"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
				</li>
				<?php endforeach; endif; ?>
				<?php wp_reset_postdata(); ?>
			</ul>
		<!-- .side-box // --></div>
	<!-- #entrySide // --></div>
<!-- #entryWrap // --></div>

<!-- #Main // --></section>
<!-- #Content // --></section>
<!-- △メインコンテンツここまで // -->

<?php get_footer(); ?>

<!-- #Page // --></div>

<?php include( TEMPLATEPATH . '/gr_tag.php' ); ?>

</body>
</html>