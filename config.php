<?php
$title = 'こぐまえん | 神戸市中央区の内閣府認定の保育園';
$keywords = '保育,保育園,こぐまえん,認定保育,認可保育,企業主導型保育,神戸,神戸市,中央区';
$description = '神戸市中央区にあるこぐまえん。医療に準ずる隈病院が運営する保育園です。こぐまえんではお子さんの健やかな生活を見守り、日々の成長を親御さまと感じ、お子様の未来を考えた保育を行っております。';

if (is_page('concept')) {
    $pagetitle = $post -> post_title;
    $title = $pagetitle . ' | ' . $title;
    $keywords = $pagetitle . ',' . $keywords;
    $description = $pagetitle . ' | ' . $description;
    $h1 = $pagetitle . ' | ' . $h1;
}
if (is_page('feature')) {
    $pagetitle = $post -> post_title;
    $title = $pagetitle . ' | ' . $title;
    $keywords = $pagetitle . ',' . $keywords;
    $description = $pagetitle . ' | ' . $description;
    $h1 = $pagetitle . ' | ' . $h1;
}
if (is_page('life')) {
    $pagetitle = $post -> post_title;
    $title = $pagetitle . ' | ' . $title;
    $keywords = $pagetitle . ',' . $keywords;
    $description = $pagetitle . ' | ' . $description;
    $h1 = $pagetitle . ' | ' . $h1;
}
if (is_page('place')) {
    $pagetitle = $post -> post_title;
    $title = $pagetitle . ' | ' . $title;
    $keywords = $pagetitle . ',' . $keywords;
    $description = $pagetitle . ' | ' . $description;
    $h1 = $pagetitle . ' | ' . $h1;
}
if (is_page('flow')) {
    $pagetitle = $post -> post_title;
    $title = $pagetitle . ' | ' . $title;
    $keywords = $pagetitle . ',' . $keywords;
    $description = $pagetitle . ' | ' . $description;
    $h1 = $pagetitle . ' | ' . $h1;
}
if (is_page('about')) {
    $pagetitle = $post -> post_title;
    $title = $pagetitle . ' | ' . $title;
    $keywords = $pagetitle . ',' . $keywords;
    $description = $pagetitle . ' | ' . $description;
    $h1 = $pagetitle . ' | ' . $h1;
}
if (is_page('contact')) {
    $pagetitle = $post -> post_title;
    $title = $pagetitle . ' | ' . $title;
    $keywords = $pagetitle . ',' . $keywords;
    $description = $pagetitle . ' | ' . $description;
    $h1 = $pagetitle . ' | ' . $h1;
}
if (is_page('privacy')) {
    $pagetitle = $post -> post_title;
    $title = $pagetitle . ' | ' . $title;
    $keywords = $pagetitle . ',' . $keywords;
    $description = $pagetitle . ' | ' . $description;
    $h1 = $pagetitle . ' | ' . $h1;
}
if (is_page('thanks')) {
    $pagetitle = $post -> post_title;
    $title = $pagetitle . ' | ' . $title;
    $keywords = $pagetitle . ',' . $keywords;
    $description = $pagetitle . ' | ' . $description;
    $h1 = $pagetitle . ' | ' . $h1;
}
if (is_page('lunch')) {
    $pagetitle = $post -> post_title;
    $title = $pagetitle . ' | ' . $title;
    $keywords = $pagetitle . ',' . $keywords;
    $description = $pagetitle . ' | ' . $description;
    $h1 = $pagetitle . ' | ' . $h1;
}
?>