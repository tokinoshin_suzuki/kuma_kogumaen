<?php
/*
Template Name: サンクス
*/
?>
<?php get_header(); ?>
<div id="Page">

<?php include( TEMPLATEPATH . '/head.php' ); ?>

<!-- ▽メインコンテンツここから // -->
<section id="Content" class="g-content">
<div class="pagettl u-pc">
<h1><img src="<?php echo get_template_directory_uri(); ?>/images/thanks/common/page_ttl.png" width="290" height="30" alt="お問合せ"></h1>
<!-- .pagettl // --></div>
<div class="pagettl u-sp">
<h1><img src="<?php echo get_template_directory_uri(); ?>/images/thanks/common/sp/page_ttl.jpg" width="640" height="180" alt="お問合せ"></h1>
<!-- .pagettl // --></div>
<section id="Main">
<div class="secttl ttl-about thanks-ttl">
  <h2>お問合せありがとうございます。</h2>
<!-- .secttl // --></div>
<div class="secbox">
<div class="item">
<p class="item-ttl">この度はお問合せ頂き誠にありがとうございます。</p>
<p class="item-txt">お問い合わせ内容を拝見後、ご連絡させて頂きますのでよろしくお願いいたします。<br>また、保育中などご返答にお時間を頂く場合がございます。ご了承いただきますようお願い致します。</p>
<!-- .item // --></div>
<!-- .secbox // --></div>


<!-- #Main // --></section>
<!-- #Content // --></section>
<!-- △メインコンテンツここまで // -->

<?php get_footer(); ?>

<!-- #Page // --></div>

<?php include( TEMPLATEPATH . '/gr_tag.php' ); ?>

</body>
</html>