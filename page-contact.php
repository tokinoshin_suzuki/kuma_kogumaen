<?php get_header(); ?>
<div id="Page">

<?php include( TEMPLATEPATH . '/head.php' ); ?>

<!-- ▽メインコンテンツここから // -->
<section id="Content" class="g-content">
<div class="pagettl u-pc">
<h1><img src="<?php echo get_template_directory_uri(); ?>/images/contact/common/page_ttl.png" width="176" height="29" alt="お問い合せ"></h1>
<!-- .pagettl // --></div>
<div class="pagettl u-sp">
<h1><img src="<?php echo get_template_directory_uri(); ?>/images/contact/common/sp/page_ttl.jpg" width="640" height="180" alt="お問い合せ"></h1>
<!-- .pagettl // --></div>
<section id="Main">

<div class="secbox">
<div class="contact-box">
<p class="contact-ttl">こぐまえんへのお問い合わせはこちら。 お返事にお時間頂戴することもあります。ご了承下さい。</p>
<?php echo do_shortcode( '[contact-form-7 id="30" title="お問い合わせ"]' ); ?>

<!-- .contact-box // --></div>

<!-- .secbox // --></div>


<!-- #Main // --></section>
<!-- #Content // --></section>
<!-- △メインコンテンツここまで // -->

<?php get_footer(); ?>

<!-- #Page // --></div>

<?php include( TEMPLATEPATH . '/gr_tag.php' ); ?>


</body>
</html>